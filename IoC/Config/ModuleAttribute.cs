﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IoC.Config {
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
    public class ModuleAttribute : Attribute {
        public ModuleAttribute() {
        }

        public ModuleAttribute(string Name) {
            this.Name = Name;
        }

        public string Name { get; private set; }
    }
}
