﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IoC.Config {
    [AttributeUsage(AttributeTargets.Property,AllowMultiple=false)]
    public class ActiveFlagAttribute : Attribute {
    }
}
