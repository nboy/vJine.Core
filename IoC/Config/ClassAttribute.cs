﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IoC.Config {
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ClassAttribute : Attribute {
        public ClassAttribute() {
        }

        public ClassAttribute(string Name) {
            this.Name = Name;
        }

        public string Name { get; set; }
        public bool IsSingle { get; set; }
    }
}
