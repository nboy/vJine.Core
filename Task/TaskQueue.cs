﻿using System;
using System.Collections.Generic;
using System.Threading;
using vJine.Core.IoC;
using vJine.Core.ORM;
using vJine.Core.AOP;

namespace vJine.Core.Task {
    public class TaskQueue<T>
        where T : class {
        const int default_capacity = 1;
        const int default_task_max = 1;

        public static TaskQueue<T> Init(int capacity, Exec<T> worker) {
            TaskQueue<T> taskQueue = new TaskQueue<T>(capacity, default_task_max, null, null, worker);
            taskQueue.Start();

            return taskQueue;
        }

        public static TaskQueue<T> Init(int capacity, int tasks, Exec<T> worker) {
            TaskQueue<T> taskQueue = new TaskQueue<T>(capacity, tasks, null, null, worker);
            taskQueue.Start();

            return taskQueue;
        }

        public static TaskQueue<T> Init(int capacity, int tasks, string keyResource, Exec<T> worker) {
            TaskQueue<T> taskQueue = new TaskQueue<T>(capacity, tasks, null, null, worker);
            taskQueue.Start();

            return taskQueue;
        }

        int Max_priority = 6;
        public TaskQueue()
            : this(10) {
        }

        int Capacity { get; set; }
        public TaskQueue(int capacity)
            : this(capacity, 1, null, null, null) {

        }

        public TaskQueue(int capacity, Exec<T> worker)
            : this(capacity, default_task_max, null, null, worker) {
        }

        public TaskQueue(int capacity, Exec service)
            : this(capacity, 1, null, service, null) {
        }

        public TaskQueue(int capacity, Exec service, Exec<T> worker)
            : this(capacity, 1, null, service, worker) {
        }

        public TaskQueue(int capacity, int task_max, string keyResource, Exec service, Exec<T> worker) {
            this.Capacity = capacity; this.task_max = task_max;
            this.keyResource = string.IsNullOrEmpty(keyResource) ? null : Class.Get(typeof(T), keyResource);
            this.service = service;
            this.worker = worker;

            this.signal_P_enqueue = new Semaphore(capacity, capacity);
            this.sinal_P_dequeue = new Semaphore(0, capacity);

            this.signal_T_enqueue = new Semaphore(capacity, capacity);
            this.sinal_T_dequeue = new Semaphore(0, capacity);

            for (int i = 0; i <= this.Max_priority; i++) {
                this.PriorityTasks.Add(i, new Queue<T>());
            }
        }

        #region Priority Queue

        Semaphore signal_P_enqueue; Semaphore sinal_P_dequeue;
        Dictionary<int, Queue<T>> PriorityTasks = new Dictionary<int, Queue<T>>();

        public void Enqueue(T data) {
            this.Enqueue(this.Max_priority, data);
        }

        public void Enqueue(int priority, T data) {
            if (priority < 0 || priority > this.Max_priority) {
                throw new CoreException("Priproty[{0}] Out Of Range(0-{1})", priority, this.Max_priority);
            }

            this.signal_P_enqueue.WaitOne();
            lock (this.PriorityTasks) {
                this.PriorityTasks[priority].Enqueue(data);
            }

            this.sinal_P_dequeue.Release();
        }

        public T Dequeue() {
            return this.Dequeue(0);
        }

        public T Dequeue(int delayms) {
            if (delayms <= 0) {
                this.sinal_P_dequeue.WaitOne();
            } else {
                if (!this.sinal_P_dequeue.WaitOne(delayms)) {
                    return null;
                }
            }

            lock (this.PriorityTasks) {
                for (int i = 0; i <= this.Max_priority; i++) {
                    if (this.PriorityTasks[i].Count > 0) {
                        T data = this.PriorityTasks[i].Dequeue();
                        this.signal_P_enqueue.Release();
                        return data;
                    }
                }
            }

            throw new CoreException("TaskQueue.Dequeue Fail");
        }
        #endregion Priority Queue

        #region Topic Queue

        Semaphore signal_T_enqueue; Semaphore sinal_T_dequeue;
        Dictionary<string, Queue<T>> TopicTasks = new Dictionary<string, Queue<T>>();

        public void Enqueue(string Topic, T data) {
            if (Topic == null) {
                throw new CoreException("Topic Can't be null");
            }

            this.signal_T_enqueue.WaitOne();
            lock (this.TopicTasks) {
                if (!this.TopicTasks.ContainsKey(Topic)) {
                    this.TopicTasks.Add(Topic, new Queue<T>());
                }
                this.TopicTasks[Topic].Enqueue(data);
            }

            this.sinal_T_dequeue.Release();
        }

        public T Dequeue(string Topic) {
            return this.Dequeue(Topic, 10);
        }

        public T Dequeue(string Topic, int delayms) {
            if (delayms <= 0) {
                this.sinal_T_dequeue.WaitOne();
            } else {
                if (!this.sinal_T_dequeue.WaitOne(delayms)) {
                    return null;
                }
            }

            lock (this.TopicTasks) {
                if (!this.TopicTasks.ContainsKey(Topic)) {
                    this.sinal_T_dequeue.Release();
                    return null;
                }

                Queue<T> tt = this.TopicTasks[Topic];

                T t = tt.Dequeue();
                if (tt.Count == 0) {
                    this.TopicTasks.Remove(Topic);
                }

                this.signal_T_enqueue.Release();
                return t;
            }
            throw new CoreException("TaskQueue.Dequeue Fail");
        }
        #endregion Topic Queue

        bool IsRunning = false;
        int task_max = 0;
        Semaphore taskSignal = null;

        public void Start() {
            this.Start(this.task_max == 0 ? default_task_max : this.task_max);
        }

        Exec service = null;
        Exec<T> worker = null;
        public void Start(int max_tasks) {

            lock (this) {
                if (this.IsRunning) {
                    throw new CoreException("Task Is In Runnig State");
                }

                this.task_max = max_tasks;

                this.taskSignal =
                    new Semaphore(max_tasks, max_tasks);
                this.IsRunning = true;

                this.asyncCallBack =
                    new AsyncCallback(this.task_complete);

                if (this.service != null) {
                    ThreadPool.QueueUserWorkItem(new WaitCallback((object objNull) => {
                        this.service();
                    }), null);
                }

                if (worker != null) {
                    Exec<Exec<T>> task = this.task_dispatcher;
                    task.BeginInvoke(worker, null, null);
                }
            }
        }

        ManualResetEvent stop_signal = new ManualResetEvent(false);
        public void Stop() {
            lock (this) {
                this.IsRunning = false;

                //等待任务停止
                this.stop_signal.WaitOne();
                this.stop_signal.Close();
                this.stop_signal = null;

                this.keyResource = null;
            }
        }

        GetJoint keyResource = null;
        Dictionary<object, object> KeyResources = new Dictionary<object, object>();
        AsyncCallback asyncCallBack = null;
        void task_dispatcher(Exec<T> worker) {
            while (this.IsRunning) {
                if (!this.taskSignal.WaitOne(100)) {
                    continue;
                }
                
                T data = null;
                do {
                    if (!this.IsRunning) {
                        return;
                    }
                    data = this.Dequeue(1000);
                }
                while (data == null);

                lock (this.KeyResources) {
                    object objKey =
                        this.keyResource == null ? data : this.keyResource.Invoke(data);
                    objKey = objKey ?? data;

                    while (this.KeyResources.ContainsKey(objKey)) { //TOFIX:阻塞直至关键资源释放
                        Thread.Sleep(100);
                    }
                    worker.BeginInvoke(data, this.asyncCallBack, objKey);
                }
            }

            this.stop_signal.Set();
        }

        void task_complete(IAsyncResult result) {
            lock (this.KeyResources) {
                object objKey = result.AsyncState;
                if (objKey != null) {
                    if (this.KeyResources.ContainsKey(objKey)) {
                        this.KeyResources.Remove(objKey);
                    }
                }
            }

            this.taskSignal.Release();
        }
    }
}