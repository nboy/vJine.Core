﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using vJine.Core.IO.Xml;
using vJine.Core.IoC;

namespace vJine.Core.Task {
    [Serializable]
    [AppConfig("vJine.Net/Cron")]
    public partial class Cron : IComparer<Cron.Job> {
        public int Compare(Cron.Job x, Cron.Job y) {
            if (x == null && y == null || x.Next == null && y.Next == null) {
                return 0;
            } else if (x == null || x.Next == null) {
                return -1;
            } else if (y == null || y.Next == null) {
                return 1;
            } else if (x == y) {
                return 0;
            } else {
                DateTime at_x = x.Next.Value; DateTime at_y = y.Next.Value;
                if (at_x > at_y) {
                    return 1;
                } else if (at_x == at_y) {
                    return 0;
                } else {
                    return -1;
                }
            }
        }

        [XmlArray("Jobs")]
        public List<Job> Jobs { get; set; }

        public Cron() {
            this.Jobs = new List<Job>();

        }

        public void Start() {
            if (this.Jobs.Count == 0) {
                return;
            }
            DateTime now = DateTime.Now;
            for (int i = 0, len = this.Jobs.Count; i < len; i++) {
                this.Jobs[i].SetNext(now);
            }
            this.Jobs.Sort(this);

            this.tmr_daemon =
                new Timer(new TimerCallback(this.task_daemon), null, 0, Timeout.Infinite);
        }

        Timer tmr_daemon = null;
        public event Exec<Job> OnTask;
        long period = Timeout.Infinite;

        void task_daemon(object obj) {
            for (int i = 0; i < this.Jobs.Count; ) {
                DateTime now = DateTime.Now;
                Job job_i = this.Jobs[i];

                if (job_i.Next == null) {
                    this.Jobs.Remove(job_i);
                    continue;
                } else if (job_i.Next <= now) {
                    this.OnTask.BeginInvoke(job_i, null, null);

                    if (job_i.SetNext(DateTime.Now) == null) {
                        this.Jobs.Remove(job_i); continue;
                    } else {
                        this.Jobs.Sort(this);
                        i = 0; continue;
                    }
                } else {
                    this.tmr_daemon.Change(job_i.get_duetime(now), this.period);
                    return;
                }
            }

            if (this.Jobs.Count > 0) {
                this.Jobs.Sort(this);
                this.tmr_daemon.Change(
                    this.Jobs[0].get_duetime(), this.period);
            }
        }

        public static void Save(Cron cron, string cron_file) {
            XmlHelper.ToString<Cron>(cron, cron_file);
        }

        public static Cron Load(string cron_file) {
            return XmlHelper.Parse<Cron>(cron_file);
        }
    }
}
