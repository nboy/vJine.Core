﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using System.Collections;
using vJine.Core.IoC;

namespace vJine.Core.Base {
    [Serializable]
    public class CollectionBase<TItem> : BindingList<TItem>, INotifyPropertyChanged, IDisposable {
        public static implicit operator TItem[](CollectionBase<TItem> items) {
            if (object.Equals(items, null)) {
                return null;
            }

            TItem[] tArray = new TItem[items.Count];
            for (int i = 0; i < items.Count; i++) {
                tArray[i] = items[i];
            }

            return tArray;
        }

        #region INotifyPropertyChanged 成员

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String pName) {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(pName));
            }
        }

        #endregion

        #region User Custom
        public CollectionBase<TItem> Add(CollectionBase<TItem> Items) {
            foreach (TItem item in Items) {
                base.Add(item);
            }

            return this;
        }

        public CollectionBase<TItem> Add(TItem[] Items) {
            foreach (TItem item in Items) {
                base.Add(item);
            }

            return this;
        }

        public new CollectionBase<TItem> Add(TItem Item) {
            base.Add(Item);

            return this;
        }

        public bool Add(TItem Item, Call<bool, TItem, TItem> comparer) {
            for (int i = 0; i < this.Count; i++) {
                if (comparer(Item, base[i])) {
                    base.RemoveAt(i); base.Add(Item);
                    return true;
                }
            }

            base.Add(Item); return false;
        }

        public bool Contains(TItem Item, Call<bool, TItem, TItem> comparer) {
            for (int i = 0; i < this.Count; i++) {
                if (comparer(Item, base[i])) {
                    return true;
                }
            }
            return false;
        }

        public CollectionBase<TItem> Clear(CollectionBase<TItem> Items) {
            this.Clear();
            this.Add(Items);

            return this;
        }

        public CollectionBase<TItem> Clear(TItem[] Items) {
            this.Clear();
            this.Add(Items);

            return this;
        }
        #endregion

        #region IDisposable 成员

        public void Dispose() {
            for (int i = 0; i < this.Count; i++) {
                this[i] = default(TItem);
            }
            this.Clear();
        }

        #endregion
    }
}
