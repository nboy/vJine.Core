﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace vJine.Core.ORM {
    public class OrmException : CoreException {
        public OrmException()
            : base() {

        }

        public OrmException(Exception ex)
            : base(ex) {

        }

        public OrmException(string Msg, params object[] Args)
            : base(Msg, Args) {

        }

        public OrmException(Exception inner, string Msg, params object[] Args)
            : base(inner, Msg, Args) {

        }

        public OrmException(MethodBase method, string Msg, params object[] Args)
            : base(method, Msg, Args) {
        }
    }
}
