﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text;

using vJine.Core.Base;
using vJine.Core.IoC;
using vJine.Core.ORM.Adapters;

namespace vJine.Core.ORM {
    public partial class DataManager : ItemBase, IDisposable {

        #region Init & Dispose

        public DataManager() : this("Default") {  
        }

        public DataManager(string name) {
            if (string.IsNullOrEmpty(name)) {
                throw new ArgumentNullException("db_name");
            }
            OrmConfig.db db_config = OrmConfig.My.Connections[name];
            if (db_config == null) {
                throw new OrmException("Connections.db[{0}] Is Null", name);
            }

            //
            this.dbProvider = db_config.get_Factory();
            this.Adapter = db_config.get_Adapter();

            //
            this.dbConn = this.dbProvider.CreateConnection();
            this.dbConn.ConnectionString = string.Format(
                db_config.connectionString,
                db_config.host, db_config.port, db_config.user, db_config.password
                );
            this.Adapter.Conn = this.dbConn;

            this.debug = OrmConfig.My.debug;
        }

        bool debug {
            get;
            set;
        }

        public IDbAdapter Adapter {
            private set;
            get;
        }

        public virtual void Dispose() {
            this.Close(true);
            this.dbConn = null;
        }

        #endregion Init & Dispose

        #region DDL
        public void Create<Tdto>(params string[] Tables)
            where Tdto : class, new() {
            this.Create<Tdto>(false, Tables);
        }

        public void Create<Tdto>(bool DropIfExists, params string[] Tables)
            where Tdto : class, new() {
                if (Tables == null || Tables.Length == 0) {
                    Tables = new string[] { Class<Tdto>.Name };
                }

                this.try_exec(() => {
                    for (int i = 0, len = Tables.Length; i < len; i++) {
                        string table_i = Tables[i];
                        if (DropIfExists) {
                            this.Drop<Tdto>(true, table_i);
                        }
                        this.InitCmd(this.Adapter.PrepareCreate<Tdto>(table_i));
                        this.dbCmd.ExecuteNonQuery();
                    }
                });
        }

        public void Drop<Tdto>(params string[] Tables) 
            where Tdto : class, new() {
            this.Drop<Tdto>(false, Tables);
        }

        public void Drop<Tdto>(bool IfExists, params string[] Tables)
            where Tdto : class, new() {
            if (Tables == null || Tables.Length == 0) {
                Tables = new string[] { Class<Tdto>.Name };
            }

            this.try_exec(() => {
                for (int i = 0, len = Tables.Length; i < len; i++) {
                    this.InitCmd(this.Adapter.PrepareDrop<Tdto>(IfExists, Tables[i]));
                    this.dbCmd.ExecuteNonQuery();
                }
            });
        }

        #endregion DDL

        #region Scalar

        public bool Exist<T>() {
            return this.Count<T>(null) > 0;
        }

        public bool Exist<T>(Class<T>.Where where) {
            return this.Count<T>(where) > 0;
        }

        public long Count<T>() {
            return this.Count<T>(null);
        }

        public long Count<T>(Class<T>.Where where) {
            this.InitCmd(
                    this.Adapter.ToWhereString<T>("Select Count(*) From " + Class<T>.Name, where)
                    );

            return this.try_exec<long>(false, () => {
                return (long)this.dbCmd.ExecuteScalar();
            });
        }

        public object GetObject(string SQL, params object[] Params) {
            object obj_return = null;

            this.try_exec(() => {
                this.InitCmd(SQL, Params);

                obj_return = this.dbCmd.ExecuteScalar();
            });

            return obj_return;
        }

        public Tscalar GetScalar<Tscalar>(string SQL, params object[] Params)
            where Tscalar : struct {
            return (Tscalar)this.GetObject(SQL, Params);
        }

        public string GetString(string SQL, params object[] Params) {
            object obj = this.GetObject(SQL, Params);
            return obj == DBNull.Value ? null : (string)obj;
        }

        public byte[] GetBytes(string SQL, params object[] Params) {
            return (byte[])this.GetObject(SQL, Params);
        }

        public string Version {
            get {
                return this.try_exec<string>(false, () => {
                    return this.dbConn.ServerVersion;
                });
            }
        }

        public DateTime Now {
            get {
                object obj_time = this.GetObject(this.Adapter.GetDateTime);
                if (obj_time is DateTime) {
                    return (DateTime)obj_time;
                } else if (obj_time is string) {
                    return DateTime.Parse((string)obj_time);
                } else {
                    throw new OrmException("DateTime Result Type Error");
                }
            }
        }
        #endregion Scalar

        #region I-D-U-Q

        #region I
        public int I<Tdto>(Tdto dto, params string[] tables)
            where Tdto : class, new() {
            int insert_counter = 0;

            IDbAdapter db_adapter = this.Adapter;
            List<Class<Tdto>.Property> keys = db_adapter.GetFields<Tdto>();
            Exec<Tdto, DbCommand> params_copy = db_adapter.Get_I<Tdto>(keys);
            if(tables == null || tables.Length == 0) {
                this.try_exec(() => {
                    this.InitCmd(
                        db_adapter.PrepareInsert<Tdto>(Class<Tdto>.Name, keys)
                        );

                    params_copy(dto, this.dbCmd);
                    insert_counter = this.dbCmd.ExecuteNonQuery();
                });
            } else {
                this.try_exec(() => {
                    for(int i = 0, len = tables.Length; i < len; i++) {
                        string table_i = tables[i];
                        this.InitCmd(
                            db_adapter.PrepareInsert<Tdto>(table_i, keys)
                            );

                        params_copy(dto, this.dbCmd);
                        insert_counter += this.dbCmd.ExecuteNonQuery();
                    }
                });
            }

            return insert_counter;
        }

        public int I<Tdto>(IList<Tdto> dtos, params string[] tables)
            where Tdto : class, new() {
            int insert_counter = 0;

            IDbAdapter db_adapter = this.Adapter;
            Class<Tdto>.Property[] keys = Class<Tdto>.Keys;
            Exec<Tdto, DbCommand> params_copy = db_adapter.Get_I<Tdto>(keys);
            if(tables == null || tables.Length == 0) {
                this.try_exec(() => {
                    this.InitCmd(
                        db_adapter.PrepareInsert<Tdto>(Class<Tdto>.Name, keys)
                        );

                    for(int j = 0, len_j = dtos.Count; j < len_j; j++) {
                        params_copy(dtos[j], this.dbCmd);
                        insert_counter += this.dbCmd.ExecuteNonQuery();
                    }
                });
            } else {
                this.try_exec(() => {
                    for(int i = 0, len = tables.Length; i < len; i++) {
                        string table_i = tables[i];
                        this.InitCmd(
                            db_adapter.PrepareInsert<Tdto>(Class<Tdto>.Name, keys)
                            );

                        for(int j = 0, len_j = dtos.Count; j < len_j; j++) {
                            params_copy(dtos[j], this.dbCmd);
                            insert_counter += this.dbCmd.ExecuteNonQuery();
                        }
                    }
                });
            }

            return insert_counter;
        }
        #endregion I

        #region D
        public int D<Tdto>(Class<Tdto>.Where Where, params string[] tables)
            where Tdto : class, new() {
            int delete_counter = 0;
            IDbAdapter db_adapter = this.Adapter;
            if(tables == null || tables.Length == 0) {
                this.InitCmd(this.Adapter.PrepareDelete<Tdto>(Class<Tdto>.Name, Where));
                this.try_exec(() => {
                    delete_counter = this.dbCmd.ExecuteNonQuery();
                });
            } else {
                this.try_exec(() => {
                    for(int i = 0, len = tables.Length; i < len; i++) {
                        this.InitCmd(this.Adapter.PrepareDelete<Tdto>(tables[i], Where));
                        delete_counter += this.dbCmd.ExecuteNonQuery();
                    }
                });
            }
            return delete_counter;
        }

        public int D<Tdto>(Tdto entity, params string[] tables)
            where Tdto : class, new() {
            Class<Tdto>.Where Where = null;
            for (int i = 0, len = Class<Tdto>.Keys.Length; i < len; i++) {
                Class<Tdto>.Property p_i = Class<Tdto>.Keys[i];
                Where &= new Class<Tdto>.Where(p_i, "=", p_i.Get(entity));
            }
            if (Where == null) {
                throw new OrmException("Where Condition Is Null");
            }

            return this.D<Tdto>(Where);
        }
        #endregion D

        #region U
        public int U<Tdto>(Tdto entity, params string[] tables)
            where Tdto : class, new() {
                return this.U<Tdto>(entity, Class<Tdto>.Keys, Class<Tdto>.Values, tables);
        }


        public int U<Tdto>(Class<Tdto>.Set Set, Class<Tdto>.Where Where, params string[] tables)
            where Tdto : class, new() {
            int update_counter = 0;
            if(tables == null || tables.Length == 0) {
                this.InitCmd(this.Adapter.PrepareUpdate<Tdto>(Class<Tdto>.Name,Set,Where));
                update_counter = this.dbCmd.ExecuteNonQuery();
            } else {
                this.try_exec(() => {
                    for(int i = 0, len = tables.Length; i < len; i++) {
                        this.InitCmd(this.Adapter.PrepareUpdate<Tdto>(tables[i], Set, Where));
                        update_counter += this.dbCmd.ExecuteNonQuery();
                    }
                });
            }
            return update_counter;
        }

        public int U<Tdto>(Tdto entity, Class<Tdto>.Property[] setProps, Class<Tdto>.Property[] whereProps, params string[] tables)
            where Tdto : class, new() {

            Class<Tdto>.Set Set = null;
            for (int i = 0, len = setProps.Length; i < len; i++) {
                Class<Tdto>.Property p_i = setProps[i];
                Set &= new Class<Tdto>.Set(p_i, "=", p_i.Get(entity));
            }
            if (Set == null) {
                throw new OrmException("Set Fields Is Null");
            }

            Class<Tdto>.Where Where = null;
            for (int i = 0, len = whereProps.Length; i < len; i++) {
                Class<Tdto>.Property p_i = whereProps[i];
                Where &= new Class<Tdto>.Where(p_i, "=", p_i.Get(entity));
            }
            if (Where == null) {
                throw new OrmException("Where Condition Is Null");
            }

            return this.U<Tdto>(entity, Set, Where, tables);

        }
        #endregion U

        #region Q
        public Tdto Q<Tdto>(Class<Tdto>.Where where, params string[] tables)
            where Tdto : class, new() {
            List<Tdto> container = new List<Tdto>();

            this.Q<Tdto>(container, where, tables);

            if(container.Count == 0) {
                return null;
            } else if(container.Count > 1) {
                throw new OrmException("Multi Results[{0}]", container.Count);
            } else {
                return container[0];
            }
        }

        public int Q<Tdto>(IList<Tdto> container, params string[] tables)
            where Tdto : class, new() {
                return this.Q<Tdto>(container, (Class<Tdto>.Where)null, tables);
        }

        public int Q<Tdto>(IList<Tdto> container, Class<Tdto>.Where where, params string[] tables)
            where Tdto : class, new() {
            List<Class<Tdto>.Property> fields = this.Adapter.GetFields<Tdto>();

            int query_coounter=0;
            IDbAdapter db_adapter = this.Adapter;
            Exec<DbDataReader, Tdto> result_copy = db_adapter.Get_Q<Tdto>(fields);
            if(tables == null || tables.Length == 0) {
                this.try_exec(() => {
                    this.InitCmd(db_adapter.PrepareQuery<Tdto>(0, fields, Class<Tdto>.Name, where, null));
                    DbDataReader reader = this.dbCmd.ExecuteReader();
                    try {
                        query_coounter = this.Fetch<Tdto>(reader, result_copy, container);
                    } finally {
                        reader.Close();
                    }
                });
            } else {
                this.try_exec(() => {
                    for(int i = 0, len = tables.Length; i < len; i++) {
                        string table_i = tables[i];
                        this.InitCmd(db_adapter.PrepareQuery<Tdto>(0, fields, table_i, where, null));
                        DbDataReader reader = this.dbCmd.ExecuteReader();
                        try {
                            query_coounter = this.Fetch<Tdto>(reader, result_copy, container);
                        } finally {
                            reader.Close();
                        }
                    }
                });
            }
            return query_coounter;
        }
        #endregion Q

        #endregion I-D-U-Q

        #region E I-D-U-Q

        public virtual int E<Tdto>(Class<Tdto>.I I)
            where Tdto : class, new() {
            int counter = 0;

            this.InitCmd(I);
            Exec<Tdto, DbCommand> params_copy = I.Pcopy;

            this.try_exec(() => {
                List<Tdto> values = I.values;
                for (int i = 0, len = values.Count; i < len; i++) {
                    Tdto value_i = values[i];
                    params_copy(value_i, this.dbCmd);

                    counter += this.dbCmd.ExecuteNonQuery();
                }
            });

            return counter;
        }

        public virtual int E<Tdto>(Class<Tdto>.D D)
            where Tdto : class, new() {
            this.InitCmd(D);

            return this.try_exec<int>(false, () => {
                return this.dbCmd.ExecuteNonQuery();
            });
        }

        public virtual int E<Tdto>(Class<Tdto>.U U)
            where Tdto : class, new() {
            this.InitCmd(U);

            return this.try_exec<int>(false, () => {
                return this.dbCmd.ExecuteNonQuery();
            });
        }

        public virtual int E<Tdto>(Class<Tdto>.Q Q, IList<Tdto> Container)
            where Tdto : class, new() {

            return this.try_exec<int>(false, () => {
                this.InitCmd(Q);

                DbDataReader reader = this.dbCmd.ExecuteReader();

                int resultCounter = 
                    this.Fetch<Tdto>(reader, Q.Qcopy, Container);

                reader.Close();

                return resultCounter;
            });
        }
        #endregion E I-D-U-Q

        #region Control Commands

        DbProviderFactory dbProvider { get; set; }
        DbConnection dbConn { get; set; }

        public string db {
            get {
                return this.dbConn.Database;
            }
            set {
                this.dbConn.ChangeDatabase(value);
            }
        }

        int connCounter = 0;
        public void Open() {
            this.connCounter += 1;
            if (this.connCounter > 1) {
                return;
            }

            this.dbConn.Open();
        }

        public void Close() {
            this.Close(false);
        }

        public void Close(bool Force) {
            this.connCounter -= 1;

            if (this.connCounter == 0 || Force) {
                this.connCounter = 0;

                this.Commit();
                this.dbConn.Close();
            }
        }
        #endregion Control Commands

        #region Transaction

        public void Begin() {
            this.Begin(IsolationLevel.Unspecified);
        }

        int transCounter = 0;
        protected DbTransaction dbTrans { get; private set; }
        public void Begin(IsolationLevel level) {
            this.transCounter++;
            if (this.transCounter > 1) {
                return;
            }

            this.dbTrans =
                this.dbConn.BeginTransaction(level);
        }

        public void Commit() {
            if (this.dbTrans == null) {
                return;
            }
            this.transCounter--;
            if (this.transCounter > 0) {
                return;
            }

            this.dbTrans.Commit();
            this.transCounter = 0;
            this.dbTrans.Dispose();
            this.dbTrans = null;
        }

        public void Rollback() {
            if (this.dbTrans == null) {
                return;
            }

            
            this.dbTrans.Rollback();
            this.transCounter = 0;

            this.dbTrans.Dispose();
            this.dbTrans = null;
        }
        #endregion Transaction

        #region Cmd Commands

        DbCommand dbCmd = null;
        protected void InitCmd(string SQL, params object[] Params) {
            this.InitCmd(CommandType.Text, SQL, Params);
        }

        protected void InitCmd(CommandType Cmd, string SQL, params object[] Params) {
            if (this.dbCmd == null) {
                this.dbCmd = this.dbConn.CreateCommand();
            }
            this.dbCmd.Transaction = this.dbTrans;

            this.dbCmd.Parameters.Clear();
            if (Params != null && Params.Length > 0 && Params.Length % 2 == 0) {
                for (int i = 0, len = Params.Length; i < len; i += 2) {
                    string name = Params[i] as string;
                    object value = Params[i + 1];
                    if (string.IsNullOrEmpty(name)) {
                        continue;
                    }
                    if (value == null) {
                        value = DBNull.Value;
                    }
                    string param_name = this.Adapter.ParamPrefix + name;
                    if (this.dbCmd.Parameters.Contains(param_name)) {
                        this.dbCmd.Parameters[param_name].Value = value;
                    } else {
                        DbParameter db_param = this.dbCmd.CreateParameter();
                        db_param.ParameterName = param_name; db_param.Value = value;
                        this.dbCmd.Parameters.Add(db_param);
                        SQL = SQL.Replace("?" + name, db_param.ParameterName);
                    }
                }
            }

            this.dbCmd.CommandType = Cmd; this.dbCmd.CommandText = SQL;
        }

        public int CmdTimeOut { get; set; }
        protected void InitCmd(IDbCmd Cmd) {
            this.dbCmd = Cmd.Prepare(this.Adapter);
            this.dbCmd.Transaction = this.dbTrans;

            if (this.CmdTimeOut > 0) {
                this.dbCmd.CommandTimeout = this.CmdTimeOut;
            } else {
                this.CmdTimeOut = this.dbCmd.CommandTimeout;
            }

            this.LastCommand = this.dbCmd.CommandText;
        }

        protected void InitCmd(DbCommand dbCmd) {
            this.dbCmd = dbCmd;
            this.dbCmd.Transaction = this.dbTrans;

            if (this.CmdTimeOut > 0) {
                this.dbCmd.CommandTimeout = this.CmdTimeOut;
            } else {
                this.CmdTimeOut = this.dbCmd.CommandTimeout;
            }

            this.LastCommand = this.dbCmd.CommandText;
        }

        public int E(string SQL, params object[] Params) {
            return this.E(SQL, false, Params);
        }

        int E(string SQL, bool IsProc, params object[] Params) {
            this.InitCmd(
                IsProc ? CommandType.StoredProcedure : CommandType.Text, SQL, Params);

            return this.try_exec<int>(false, () => {
                return this.dbCmd.ExecuteNonQuery();
            });
        }
        #endregion Cmd Commands

        #region Helpers

        int Fetch<Tdto>(DbDataReader reader, Class<Tdto>.Property[] P, IList<Tdto> data)
            where Tdto : class, new() {
            return this.Fetch<Tdto>(reader, this.Adapter.Get_Q<Tdto>(P), data);
        }

        int Fetch<Tdto>(DbDataReader reader, Exec<DbDataReader, Tdto> Qcopy, IList<Tdto> data)
            where Tdto : class, new() {

            if(this.debug) {
                this.debug_show_types<Tdto>(reader);
            }

            int counter = 0;
            do {
                while (reader.Read()) {
                    Tdto dtoNew = new Tdto();
                    Qcopy(reader, dtoNew);
                    data.Add(dtoNew);

                    counter += 1;
                }
            } while (reader.NextResult());

            return counter;
        }

        void debug_show_types<T>(DbDataReader reader) {
            StringBuilder sbTypes = new StringBuilder();
            sbTypes.Append(Class<T>.FullName + "\r\n");
            for (int i = 0, len = reader.FieldCount; i < len; i++) {
                sbTypes.AppendFormat("{0}:{1}\r\n", reader.GetName(i), reader.GetFieldType(i).Name);
            }
            System.Console.WriteLine(sbTypes.ToString());
        }

        #endregion Helpers

        string LastCommand = null;
        public override string ToString() {
            if (string.IsNullOrEmpty(this.LastCommand)) {
                return "--No Command";
            }

            return this.LastCommand;
        }

        public void Bind<T>(CollectionBase<T> data)
            where T : class, INotifyPropertyChanged, new() {

            CollectionBase<T> dataCache = new CollectionBase<T>();
            dataCache.Add(data);

            data.ListChanged +=
                new ListChangedEventHandler((object sender, ListChangedEventArgs e) => {

                    switch (e.ListChangedType) {
                        case ListChangedType.ItemAdded:
                            lock (data) {
                                this.I<T>(data[e.NewIndex]);
                                if (e.NewIndex == dataCache.Count) {
                                    dataCache.Add(data[e.NewIndex]);
                                } else {
                                    dataCache.Insert(e.NewIndex, data[e.NewIndex]);
                                }
                            }
                            break;
                        case ListChangedType.ItemDeleted:
                            lock (data) {
                                try {
                                    this.D<T>(dataCache[e.NewIndex]);
                                } finally {
                                    dataCache.RemoveAt(e.NewIndex);
                                }
                            }

                            break;
                        case ListChangedType.ItemChanged:
                            Class<T>.Property p_changed =
                                Class<T>.GetProperty(e.PropertyDescriptor.Name);
                            if (p_changed == null) {
                                throw new OrmException("Property[{0}] Not Found.", e.PropertyDescriptor.Name);
                            }
                            lock (data) {
                                this.U<T>(dataCache[e.NewIndex], p_changed, Class<T>.Keys);
                            }

                            break;
                        case ListChangedType.ItemMoved:
                            lock (data) {
                                T data_temp = dataCache[e.OldIndex];
                                dataCache[e.OldIndex] = dataCache[e.NewIndex];
                                dataCache[e.NewIndex] = data_temp;
                            }
                            break;
                        default:
                            break;
                    }
                });
        }

        public void Bind<T>(T data)
            where T: class, INotifyPropertyChanged, new() {

            Class<T>.Property[] _keys = Class<T>.Keys;
            if (_keys == null || _keys.Length == 0) {
                throw new OrmException("No Primary Keys To Bind. Type:[{0}]", Class<T>.FullName);
            }

            data.PropertyChanged += (object sender, PropertyChangedEventArgs e) => {
                Class<T>.Property p_changed = Class<T>.GetProperty(e.PropertyName);
                if (p_changed == null) {
                    throw new OrmException("Property[{0}] Not Found In Type[{1}]", e.PropertyName, Class<T>.FullName);
                }
                lock (data) {
                    this.U<T>(data, _keys, new Class<T>.Property[] { p_changed });
                }
            };
        }

        void try_exec(Exec do_work) {
            this.Open();
            try {
                do_work();
            } catch(Exception ex) {
                throw ex;
            } finally {
                this.Close();
            }
        }

        T try_exec<T>(bool IsTrans, Call<T> do_work) {
            this.Open();
            try {
                if (IsTrans) {
                    this.Begin();
                }
                if (this.dbTrans != null) {
                    this.dbCmd.Transaction = this.dbTrans;
                }
                T result = do_work();
                if (IsTrans) {
                    this.Commit();
                }
                return result;
            } catch (Exception ex) {
                if (IsTrans) {
                    this.Rollback();
                }

                throw ex;
            } finally {
                this.Close();
            }
        }
    }
}
