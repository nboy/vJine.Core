﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.ORM {
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class KeyAttribute : Attribute {
        public bool IsPrimary { get; set; }
        public string GroupName { get; set; }
    }
}
