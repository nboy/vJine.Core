﻿using System;
using System.Data;
using System.Reflection;

using vJine.Core.IoC;

namespace vJine.Core.ORM {
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class MapAttribute : Attribute {

        internal OrmException initEx { get; set; }

        public MapAttribute() {
        }

        public MapAttribute(string Alias) {
            this.Alias = Alias;
        }

        public string Name { get; set; }
        internal string Alias { get; set; }

        internal string name_alias { get; set; }

        public bool IsIgnored { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsNullable { get; set; }

        public bool TrimString { get; set; }

        public string SQL_TYPE { get; set; }
        public DbType? DbType { get; set; }

        private Type _Conv;
        public Type Conv {
            get {
                return this._Conv;
            }
            set {
                if (value == null) {
                    this._Conv = null; this.Conv_I = null; this.Conv_Q = null;
                    return;
                }

                bool HasPropertyName = false;

                this.Conv_I = 
                    GetMethod(value, "CONV_I", ref HasPropertyName);
                this.I_HasPropertyName = HasPropertyName;

                this.Conv_Q = 
                    GetMethod(value, "CONV_Q", ref HasPropertyName);
                this.Q_HasPropertyName = HasPropertyName;

                this._Conv = value;
            }
        }

        public bool I_HasPropertyName { get; internal set; }
        public MethodInfo Conv_I { get; internal set; }
        public bool Q_HasPropertyName { get; internal set; }
        public MethodInfo Conv_Q { get; internal set; }

        static readonly Type[] convArgs = new Type[] { Reflect.@object };
        static readonly Type[] convArgsWithName = new Type[] { Reflect.@object, Reflect.@string };
        static MethodInfo GetMethod(Type Converter, string Name, ref bool HasPropertyName) {

            HasPropertyName = false;
            MethodInfo converter = Converter.GetMethod(Name, convArgs);
            if (converter != null) {
                return converter;
            }

            converter = Converter.GetMethod(Name, convArgsWithName);
            if (converter != null) {
                HasPropertyName = true;
            }
            return converter;
        }
    }

    public interface IConverter<T> {
        object CONV_I(object V);
        T CONV_Q(object V);
    }
}
