﻿using System;
using System.Collections.Generic;
using System.Text;

using vJine.Core.IoC;
using System.Data.Common;
using System.Reflection;
using System.Reflection.Emit;
using System.Data;

namespace vJine.Core.ORM {
    public partial class OrmConfig {

        public class Cache<Tadapter>
            where Tadapter : IDbAdapter
        {
            static Dictionary<Type, MapAttribute> Maps = new Dictionary<Type, MapAttribute>();

            public static void InitDefaultMap(Type type, string SQL_TYPE, DbType dbType) {
                InitDefaultMap(type, SQL_TYPE, dbType, null);
            }

            public static void InitDefaultMap(Type type, string SQL_TYPE, DbType dbType, Type Conv) {
                MapAttribute map = new MapAttribute() { SQL_TYPE = SQL_TYPE, DbType = dbType, Conv = Conv };
                if (!Maps.ContainsKey(type)) {
                    Maps.Add(type, map);
                } else {
                    Maps[type] = map;
                }
            }

            internal static MapAttribute getDefaultMap(Type type) {
                if (!Maps.ContainsKey(type)) {
                    return null;
                }

                return Maps[type];
            }
        }

        public class Cache<Tadapter, Tschema>
            where Tadapter : IDbAdapter
            //where Tschema : class
        {
            static Cache() {
                Cache<Tadapter, Tschema>.Init(OrmConfig.My);
                Cache<Tadapter, Tschema>.I =
                    Emit.DataManager.I<Tadapter, Tschema>(Cache<Tadapter,Tschema>.DbFields);
                Cache<Tadapter, Tschema>.Q =
                    Emit.DataManager.Q<Tadapter, Tschema>(Cache<Tadapter, Tschema>.DbFields);
            }

            public static readonly List<Class<Tschema>.Property> DbFields = new List<Class<Tschema>.Property>();
            static Dictionary<string, MapAttribute> Maps = new Dictionary<string, MapAttribute>();

            static void Init(OrmConfig config) {
                Class<Tschema>.Property[] P = Class<Tschema>.GetMap();

                for (int i = 0, len = P.Length; i < len; i++) {
                    Class<Tschema>.Property p_i = P[i];
                    Type p_Type = p_i.IsNullable ? p_i.pTypeNull : p_i.pType;
                    if (p_i.IsEnum) {
                        p_Type = p_i.UnderlyingType;
                    }
                    if (!p_i.IsPrimitive) {
                        continue;
                    }
                    MapAttribute map = getMergedMap(p_i.Name,
                        Cache<Tadapter>.getDefaultMap(p_Type),
                        config.getTypeMap<Tadapter, Tschema>(p_i),
                        getSpecificMap(p_i),
                        config.GetSchemaMap<Tadapter, Tschema>(p_i)
                        );
                    if (map.IsIgnored) {
                        continue;
                    }

                    if (string.IsNullOrEmpty(map.Alias)) {
                        map.Alias = map.Name;
                    }
                    if (map.Name != map.Alias) {
                        map.name_alias = map.Alias + " As " + map.Name;
                    } else {
                        map.name_alias = map.Name;
                    }

                    Cache<Tadapter,Tschema>.DbFields.Add(p_i); 
                    Cache<Tadapter,Tschema>.Maps.Add(p_i.Name, map);
                }
            }

            static MapAttribute getMergedMap(string map_name, params MapAttribute[] maps) {
                MapAttribute map = new MapAttribute() {
                    Name = map_name
                };

                for (int i = 0, len = maps.Length; i < len; i++) {
                    MapAttribute map_i = maps[i];
                    if (map_i == null) {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(map_i.Alias)) {
                        map.Alias = map_i.Alias;
                    }
                    if (!string.IsNullOrEmpty(map_i.SQL_TYPE)) {
                        map.SQL_TYPE = map_i.SQL_TYPE;
                    }
                    if (map_i.Conv_I != null) {
                        map.Conv_I = map_i.Conv_I;
                        map.I_HasPropertyName = map_i.I_HasPropertyName;
                    }
                    if (map_i.Conv_Q != null) {
                        map.Conv_Q = map_i.Conv_Q;
                        map.Q_HasPropertyName = map_i.Q_HasPropertyName;
                    }
                    if(map_i.DbType != null) {
                        map.DbType = map_i.DbType;
                    }
                    map.IsIgnored = map_i.IsIgnored;
                    map.IsPrimary = map_i.IsPrimary;
                    map.IsNullable = map_i.IsNullable;
                    map.TrimString = map_i.TrimString;
                }
                return map;
            }

            static MapAttribute getSpecificMap(Property p) {
                MapAttribute[] M = Reflect.GetAttribute<MapAttribute>(p.This);
                if (M.Length > 0) {
                    MapAttribute m = M[0];
                    m.Name = p.Name;
                    return m;
                } else {
                    return null;
                }
            }

            public static List<MapAttribute> GetMap() {
                List<MapAttribute> maps = new List<MapAttribute>();
                foreach (KeyValuePair<string, MapAttribute> kv in Cache<Tadapter, Tschema>.Maps) {
                    maps.Add(kv.Value);
                }
                return maps;
            }

            public static MapAttribute GetMap(Class<Tschema>.Property p) {
                string propName = p.Name;

                Dictionary<string, MapAttribute> maps = Cache<Tadapter, Tschema>.Maps;
                if (!maps.ContainsKey(propName)) {
                    return null;
                }

                return maps[propName];
            }

            static readonly Exec<Tschema, DbCommand> I;
            public static Exec<Tschema, DbCommand> Get_I(IList<Class<Tschema>.Property> P) {
                if (P == Cache<Tadapter, Tschema>.DbFields || P == null || P.Count == 0) {
                    return Cache<Tadapter, Tschema>.I;
                }

                return Emit.DataManager.I<Tadapter, Tschema>(P);
            }

            static readonly Exec<DbDataReader, Tschema> Q;
            public static Exec<DbDataReader, Tschema> Get_Q(IList<Class<Tschema>.Property> P) {
                if (P == Cache<Tadapter, Tschema>.DbFields || P == null || P.Count == 0) {
                    return Cache<Tadapter, Tschema>.Q;
                }

                return Emit.DataManager.Q<Tadapter, Tschema>(P);
            }
        }
    }
}
