﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace vJine.Core.ORM {
    public interface IDbCmd {
        DbCommand Prepare(IDbAdapter provider);
        bool IsPrepared { get; set; }
    }
}
