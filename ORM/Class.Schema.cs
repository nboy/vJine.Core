﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

using vJine.Core.AOP;
using vJine.Core.IO;
using vJine.Core.IO.Json;
using vJine.Core.IoC;
using vJine.Core.IoC.Config;

namespace vJine.Core.ORM {
    public partial class Class<T> {
        public static readonly string Name;
        public static readonly string FullName;
        public static readonly string AssemblyQualifiedName;
        public static readonly Type type;

        public static Property<T, bool>[] ActiveFlags { get; protected set; }
        public static Class<T>.Property[] Keys { get; protected set; } //Primary Keys
        public static Dictionary<string, List<Class<T>.Property>> Uniques { get; protected set; } //Unique Keys
        public static Class<T>.Property[] Values { get; protected set; } //Except Primary Keys
        public static readonly InitJoint<T> Ctor = null;

        public static implicit operator Class<T>.Property[](Class<T> C) {
            return Class<T>.Columns;
        }

        public static implicit operator Class<T>.Property(Class<T> C) {
            Property[] P = Class<T>.Columns; Property p = null;

            for (int i = 0, len = P.Length; i < len; i++) {
                if (p == null) {
                    p = P[i]; continue;
                }

                p = new Property(p, ",", P[i]);
            }

            return p;
        }

        static Dictionary<string, Property> Properties = new Dictionary<string, Property>();
        static void Init_Properties() {
            PropertyInfo[] P =
                Reflect.GetProperties<T>(BindingFlags.Public | BindingFlags.Instance);

            for (int i = 0, len = P.Length; i < len; i++) {
                PropertyInfo p = P[i];
                string pName = p.Name;

                ParameterInfo[] pis = p.GetIndexParameters();
                if (pis != null && pis.Length > 0) { //Ignore Item(Index),Count...
                    continue;
                }
                Class<T>.Properties.Add(pName, new Property(p/*, i*/));
            }

            Class<T>.Columns = new Property[Class<T>.Properties.Count];
            Properties.Values.CopyTo(Columns, 0);
        }

        static Class() {
            Type Tclass = typeof(T); Class<T>.type = Tclass;

            Class<T>.AssemblyQualifiedName = Tclass.AssemblyQualifiedName;
            Class<T>.FullName =
                Regex.Replace(Class<T>.AssemblyQualifiedName, @", Version=[^\]]+", "", RegexOptions.IgnoreCase);

            ClassAttribute[] C = Reflect.GetAttribute<ClassAttribute>(Tclass);
            if (C == null || C.Length == 0) {
                Class<T>.Name = Tclass.Name;
            } else {
                Class<T>.Name = C[0].Name;
                if (string.IsNullOrEmpty(Class<T>.Name)) {
                    Class<T>.Name = Tclass.Name;
                }
            }

            Init_Properties();

            Property[] P = Class<T>.GetMap();

            Type T_ActiveFlag = typeof(ActiveFlagAttribute);
            Type T_KeyAttribute = typeof(KeyAttribute);
            List<Property<T, bool>> _ActiveFlags = new List<Property<T, bool>>();

            List<Property> _Keys = new List<Property>();
            Dictionary<string,List<Class<T>.Property>> _Uniques = new Dictionary<string,List<Property>>();
            List<Property> _Values = new List<Property>();

            for (int i = 0, len = P.Length; i < len; i++) {
                Property p_i = P[i];

                if (p_i.IsBool && Reflect.HasAttribute(p_i, T_ActiveFlag, true)) {
                    _ActiveFlags.Add(new Property<T, bool>(p_i.Name));
                }

                KeyAttribute[] tags = Reflect.GetAttribute<KeyAttribute>(p_i);

                int len_tag = tags.Length;
                //Primary
                bool IsPrimary = false;
                for (int i_tag = 0; i_tag < len_tag; i_tag++) {
                    KeyAttribute tag_i = tags[i_tag];
                    if (tag_i.IsPrimary) {
                        IsPrimary = true;
                        _Keys.Add(p_i);
                        break;
                    }
                }
                if (!IsPrimary) {
                    _Values.Add(p_i);
                }
                //Unique
                for (int i_tag = 0; i_tag < len_tag; i_tag++) {
                    KeyAttribute tag_i = tags[i_tag];
                    if (!tag_i.IsPrimary) {
                        string group_name = tag_i.GroupName ?? "";
                        if (!_Uniques.ContainsKey(group_name)) {
                            _Uniques.Add(group_name, new List<Property>());
                        }
                        _Uniques[group_name].Add(p_i);
                    }
                }
            }

            Class<T>.ActiveFlags = _ActiveFlags.ToArray();
            Class<T>.Keys = _Keys.ToArray();
            Class<T>.Uniques = _Uniques;
            Class<T>.Values = _Values.ToArray();

            Class<T>.Ctor = new InitJoint<T>();

            //Class<T>.defaultComparer = Emit.Comparer<T>();
        }

        public static InitSignature<T> Init(params Type[] Tparams) {
            return Emit.GenCtor<T>(Tparams);
        }

        static Property[] Columns = null;

        public static Property[] GetMap(params string[] Names) {
            if (Names == null || Names.Length == 0) {
                return Columns;
            }

            List<Property> C = new List<Property>();
            for (int i = 0, len = Names.Length; i < len; i++) {
                string Name = Names[i];
                if (!Class<T>.Properties.ContainsKey(Name)) {
                    continue;
                }

                C.Add(Class<T>.Properties[Name]);
            }

            return C.ToArray();
        }

        public static Property[] GetExtraMap(params string[] P) {
            List<Property> C = new List<Property>();

            foreach (KeyValuePair<string, Property> kv in Properties) {
                bool Exclude = false; string Name = kv.Key;
                for (int j = 0, len_j = P.Length; j < len_j; j++) {
                    if (Name == P[j]) {
                        Exclude = true;
                        break;
                    }
                }
                if (Exclude) {
                    continue;
                }

                C.Add(kv.Value);
            }

            return C.ToArray();
        }

        public static Property GetProperty(string Name) {
            Property[] P = Class<T>.GetMap(Name);
            if (P == null || P.Length == 0) {
                return null;
            }

            return P[0];
        }

        public static Property[] GetMap(DbDataReader reader) {
            List<Property> C = new List<Property>();

            for (int i = 0, len = reader.FieldCount; i < len; i++) {
                string fName = reader.GetName(i);
                if (!Properties.ContainsKey(fName)) {
                    throw new CoreException("Field not found:" + fName);
                }

                C.Add(Properties[fName]);
            }

            return C.ToArray();
        }
    }

    //TODO:Instance Manager
    public partial class Class<T> {
        public static void Prepare(int capacity) {

        }

        public static T Create() {
            InitJoint<T> ctor = Class<T>.Ctor;
            if (ctor == null) {
                throw new CoreException("类【{0}】缺少无参构造函数", Class<T>.FullName);
            }
            return ctor.Invoke(null);
        }

        public static void Collect(T obj) {

        }
    }

    //ToString & Parse
    public partial class Class<T> {
        public static string ToString(T jsonObject) {
            return JsonHelper.ToString<T>(jsonObject);
        }

        public static void ToString(T jsonObject, Stream jsonStream) {
            JsonHelper.ToString<T>(jsonObject, jsonStream);
        }

        public static T Parse(string jsonString) {
            return JsonHelper.Parse<T>(jsonString);
        }

        public static T Parse(Stream jsonStream) {
            return JsonHelper.Parse<T>(jsonStream);
        }

        public static void Parse(Stream jsonStream, T jsonObject) {
            JsonHelper.Parse<T>(jsonStream, jsonObject);
        }
    }

    public partial class Class<T> {
        static readonly Call<int, T, T> defaultComparer = null;
        public static void Sort(IList<T> data) {
            if (Class<T>.defaultComparer == null) {
                Class<T>.Sort(data, Emit.Comparer<T>(Class<T>.GetMap())); return;
                //throw new CoreException("default Comparer Is Null:{0}", Class<T>.FullName);
            }

            Class<T>.Sort(data, Class<T>.defaultComparer);
        }

        public static void Sort(IList<T> data, params Class<T>.Property[] P) {
            Class<T>.Sort(data, Emit.Comparer<T>(P));
        }

        public static void Sort(IList<T> data, Call<int, T, T> comparer) {

            int min = 0, max = 0, mid = 0;
            int r_compare = 0, index_prev = 0;
            T data_i = default(T), data_prev = default(T), data_mid = default(T);
            
            for (int i = 1, len = data.Count; i < len; i++) {

                data_i = data[i];
                
                if (i == 1) {
                    min = 0; max = i - 1;
                } else {
                    if (comparer(data_i, data_prev) >= 0) {
                        min = index_prev; max = i - 1;
                    } else {
                        min = 0;  max = index_prev;
                    }
                }

                do {
                    mid = (min + max) / 2; data_mid = data[mid];
                    r_compare = comparer(data_i, data_mid);

                    if (r_compare > 0) {
                        min = mid + 1;
                    } else if (r_compare < 0) {
                        max = mid - 1;
                    } else {
                        break;
                    }
                } while (min <= max);


                index_prev = r_compare > 0 ? mid + 1 : mid;
                data_prev = data_i;

                data.RemoveAt(i);
                data.Insert(index_prev, data_i);
            }
        }

        public static string CheckSort(IList<T> data) {
            return Class<T>.CheckSort(data, null);
        }

        static string checkPropName = null; static readonly object checkLock = new object();
        public static string CheckSort(IList<T> data, params Class<T>.Property[] P) {
            lock (Class<T>.checkLock) {
                Call<int, T, T> comparer =
                    Emit.Comparer<T>((string name, int r, object arg1, object arg2) => {
                        Class<T>.checkPropName /**/ = name;
                        //TODO:内部变量不可以在代理中引用，程序报公共语言运行时错误
                    }, P);

                for (int i = 1, len = data.Count; i < len; i++) {
                    if (comparer(data[i - 1], data[i]) > 0) {
                        return string.Format("Name:{0},{1} - {2}", Class<T>.checkPropName, i - 1, i);
                    }
                }

                return null;
            }
        }
    }

    //Property
    public partial class Class<T> {
        public class Property : vJine.Core.IoC.Property {
            public static implicit operator Property[](Property p) {
                return new Property[] { p };
            }

            public static implicit operator Property(Property[] P) {
                if (P == null || P.Length != 1) {
                    throw new CoreException(MethodBase.GetCurrentMethod(), "Fail To Convert");
                }

                return P[0];
            }

            internal Property(object P1, string Op, object P2)
                : base(P1, Op, P2) {
            }

            internal Property(PropertyInfo p/*, int index*/)
                : base(p/*, index*/) {
            }

            internal Property(Property P)
                : base(P) {
            }

            public override string ToString() {
                return this.Name;
            }
        }
    }

    //Set
    public partial class Class<T> {
        public class Set : Class<T>.Property {

            public static Set operator &(Set S1, Set S2) {
                if (S1 == null && S2 == null) {
                    return null;
                } else if (S1 == null) {
                    return S2;
                } else if (S2 == null) {
                    return S1;
                } else {
                    return new Set(S1, ",", S2);
                }
            }

            internal Set(object L, string Op, object R)
                : base(L, Op, R) {
            }
        }
    }

    //Where
    public partial class Class<T> {
        public class Where : Class<T>.Property {
            public class _ {
                public static readonly Property<Where, object> L = new Property<Where, object>("L");
                public static readonly Property<Where, string> Op = new Property<Where, string>("Op");
                public static readonly Property<Where, object> R = new Property<Where, object>("R");
            }

            public static Where operator &(Where W1, Where W2) {
                if (W1 == null && W2 == null) {
                    return null;
                } else if (W1 == null) {
                    return W2;
                } else if (W2 == null) {
                    return W1;
                } else {
                    return new Where(W1, "and", W2);
                }
            }

            public static Where operator |(Where W1, Where W2) {
                if (W1 == null && W2 == null) {
                    return null;
                } else if (W1 == null) {
                    return W2;
                } else if (W2 == null) {
                    return W1;
                } else {
                    return new Where(W1, "or", W2);
                }
            }

            internal Where(Property P)
                : base(P) {
            }

            internal Where(object L, string Op, object R)
                : base(L, Op, R) {
            }
        }
    }

    public partial class Property<T, P> : Class<T>.Property {
        GetSignature<T, P> getter;
        SetSignature<T, P> setter;

        public Property(Class<T>.Property P)
            : base(P) {
            this.Init_Property();
        }

        public Property(string Name)
            : base(Class<T>.GetMap(Name)) {
            this.Init_Property();
        }

        public Property(string Name, bool init_Helper)
            : base(Class<T>.GetMap(Name)) {
            this.Init_Property(init_Helper);
        }

        internal Property(object L, string Op, object R)
            : base(L, Op, R) {
        }

        void Init_Property() {
            this.Init_Property(true);
        }

        void Init_Property(bool init_helper) {
            if (init_helper && getter == null) {
                getter = Property<T, P>.Get(this);
                setter = Property<T, P>.Set(this);
            }
        }

        public static Property<T, P> Create(string Name) {
            return new Property<T, P>(Name);
        }

        public static void Collect(Property<T, P> p) {

        }

        public static GetSignature<T, P> Get(Class<T>.Property P) {
            return Emit.GenGetter<T, P>(P);
        }

        public static GetSignature<T, P> Get(string Name) {
            return Emit.GenGetter<T, P>(Name);
        }

        public static SetSignature<T, P> Set(Class<T>.Property P) {
            return Emit.GenSetter<T, P>(P);
        }

        public static SetSignature<T, P> Set(string Name) {
            return Emit.GenSetter<T, P>(Name);
        }

        public P Get(T ctx) {
            return getter(ctx);
        }

        public void Set(T ctx, P v) {
            setter(ctx, v);
        }
    }

    public partial class Property<T, P> : Class<T>.Property {
        public Class<T>.Set EQ(Class<T>.Where P) {
            return new Class<T>.Set(this, "=", P);
        }

        public Class<T>.Set EQ(Property<T, P> P) {
            return new Class<T>.Set(this, "=", P);
        }

        public Class<T>.Set EQ(P V) {
            return new Class<T>.Set(this, "=", V);
        }

        public Class<T>.Where Like(string V) {
            if (V == null) {
                throw new ArgumentNullException("V");
            }
            if (!Reflect.IsString(this.pType)) {
                throw new ArgumentException("Type MissMatch", "V");
            }

            return new Class<T>.Where(this, "like", V);
        }

        public Class<T>.Where NotLike(string V) {
            if (V == null) {
                throw new ArgumentNullException("V");
            }
            if (!Reflect.IsString(this.pType)) {
                throw new ArgumentException("Type MissMatch", "V");
            }

            return new Class<T>.Where(this, "not like", V);
        }

        public Class<T>.Where IsNull() {
            if (!this.IsNullable) {
                throw new CoreException("{0} Is Not Nullable", this.Name);
            }

            return new Class<T>.Where(this, "is", "null");
        }

        public Class<T>.Where IsNotNull() {
            if (!this.IsNullable) {
                throw new CoreException("{0} Is Not Nullable", this.Name);
            }

            return new Class<T>.Where(this, "is", "not null");
        }

        public Class<T>.Property ASC() {
            return new Class<T>.Property(this) { IsASC = true };
        }

        public Class<T>.Property DESC() {
            return new Class<T>.Property(this) { IsASC = false };
        }

        #region Operators

        public static Class<T>.Where operator &(Property<T, P> C1, Class<T>.Property C2) {
            return new Class<T>.Where(C1, "and", C2);
        }

        public static Class<T>.Where operator |(Property<T, P> C1, Class<T>.Property C2) {
            return new Class<T>.Where(C1, "or", C2);
        }

        public static Class<T>.Where operator ==(Property<T, P> C1, Property<T, P> C2) {
            return new Class<T>.Where(C1, "=", C2);
        }

        public static Class<T>.Where operator !=(Property<T, P> C1, Property<T, P> C2) {
            return new Class<T>.Where(C1, "!=", C2);
        }

        public static Class<T>.Where operator ==(Property<T, P> C1, P V2) {
            return new Class<T>.Where(C1, "=", V2);
        }

        public static Class<T>.Where operator !=(Property<T, P> C1, P V2) {
            return new Class<T>.Where(C1, "!=", V2);
        }

        public static Class<T>.Where operator >(Property<T, P> C1, Property<T, P> C2) {
            return new Class<T>.Where(C1, ">", C2);
        }

        public static Class<T>.Where operator >(Property<T, P> C1, P V2) {
            return new Class<T>.Where(C1, ">", V2);
        }

        public static Class<T>.Where operator >=(Property<T, P> C1, Property<T, P> C2) {
            return new Class<T>.Where(C1, ">=", C2);
        }

        public static Class<T>.Where operator >=(Property<T, P> C1, P V2) {
            return new Class<T>.Where(C1, ">=", V2);
        }

        public static Class<T>.Where operator <(Property<T, P> C1, Property<T, P> C2) {
            return new Class<T>.Where(C1, "<", C2);
        }

        public static Class<T>.Where operator <(Property<T, P> C1, P V2) {
            return new Class<T>.Where(C1, "<", V2);
        }

        public static Class<T>.Where operator <=(Property<T, P> C1, Property<T, P> C2) {
            return new Class<T>.Where(C1, "<=", C2);
        }

        public static Class<T>.Where operator <=(Property<T, P> C1, P V2) {
            return new Class<T>.Where(C1, "<=", V2);
        }

        public static Class<T>.Where operator +(Property<T, P> C1, Property<T, P> C2) {
            return new Class<T>.Where(C1, "+", C2);
        }

        public static Property<T, P> operator +(Property<T, P> C1, P V2) {
            return new Property<T, P>(C1, "+", V2);
        }

        public static Property<T, P> operator -(Property<T, P> C1, Property<T, P> C2) {
            return new Property<T, P>(C1, "-", C2);
        }

        public static Property<T, P> operator -(Property<T, P> C1, P V2) {
            return new Property<T, P>(C1, "-", V2);
        }

        public static Property<T, P> operator *(Property<T, P> C1, Property<T, P> C2) {
            return new Property<T, P>(C1, "*", C2);
        }

        public static Property<T, P> operator *(Property<T, P> C1, P V2) {
            return new Property<T, P>(C1, "*", V2);
        }

        public static Property<T, P> operator /(Property<T, P> C1, Property<T, P> C2) {
            return new Property<T, P>(C1, "/", C2);
        }

        public static Property<T, P> operator /(Property<T, P> C1, P V2) {
            return new Property<T, P>(C1, "/", V2);
        }

        public static Property<T, P> operator %(Property<T, P> C1, Property<T, P> C2) {
            return new Property<T, P>(C1, "%", C2);
        }

        public static Property<T, P> operator %(Property<T, P> C1, P V2) {
            return new Property<T, P>(C1, "%", V2);
        }
        #endregion Operators

        public override bool Equals(object obj) {
            return base.Equals(obj);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}
