﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class Oracle : IDbAdapter {

        static Oracle() {
            Oracle.Init_Keywords();
            Oracle.Init_TypeMap();
        }

        public Oracle() {
            this.Name = Class<Oracle>.Name;

            this.ParamPrefix = ":";
            this.GetDateTime = "select sysdate from dual";

            this.NULL = "";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = Oracle._KeyWords;
        }

        public override List<MapAttribute> GetMap<Tschema>() {
            return OrmConfig.Cache<Oracle, Tschema>.GetMap();
        }

        public override MapAttribute GetMap<Tschema>(Class<Tschema>.Property p) {
            return OrmConfig.Cache<Oracle, Tschema>.GetMap(p);
        }

        public override Exec<Tschema, DbCommand> Get_I<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<Oracle, Tschema>.Get_I(P);
        }

        public override Exec<DbDataReader, Tschema> Get_Q<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<Oracle, Tschema>.Get_Q(P);
        }

        public override List<Class<Tschema>.Property> GetFields<Tschema>() {
            return OrmConfig.Cache<Oracle, Tschema>.DbFields;
        }

        public override DbCommand PrepareDrop<Tdto>(bool IfExists, string table_name) {

            table_name = table_name.ToUpper();

            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;
            if(IfExists) {
                dbCmd.CommandText = "declare tbl_exists number;" +
                                    "begin " +
                                    "select count(*) into tbl_exists from user_tables where table_name='" + table_name + "';" +
                                    "if tbl_exists > 0 then execute immediate 'drop table " + table_name + " purge'; end if;" +
                                    "end;";
            } else {
                dbCmd.CommandText = "DROP TABLE " + table_name;
            }

            return dbCmd;
        }

        public override DbCommand PrepareQuery<Tdto>(int max, List<Class<Tdto>.Property> fields, string table_name, Class<Tdto>.Where where, List<Class<Tdto>.Property> orders) {
            int pCounter = 0;
            StringBuilder sbCmd = new StringBuilder();
            DbCommand dbCmd = this.Conn.CreateCommand();

            sbCmd.Append("Select");

            //Fields
            this.ToSelectString<Tdto>(dbCmd, sbCmd, fields);

            sbCmd.Append(" From ").Append(table_name);

            //Where
            if(where != null) {
                sbCmd.Append(" Where ");
                this.ToWhereString<Tdto>(
                    dbCmd, sbCmd, where, ref pCounter, false);
            }

            //Order By
            if(orders != null && orders.Count > 0) {
                sbCmd.Append(" Order By ");
                for(int j = 0, len_j = orders.Count - 1; j <= len_j; j++) {
                    Class<Tdto>.Property order_j = orders[j];
                    if(order_j.IsASC == null) {
                        continue;
                    }
                    sbCmd.Append(order_j.Name).Append(order_j.IsASC.Value ? " ASC" : " DESC");
                    if(j < len_j) {
                        sbCmd.Append(",");
                    }
                }
            }

            if (max > 0) {
                dbCmd.CommandText = "Select * From (" + sbCmd.ToString() + ") Where rownum <=" + max.ToString();
            } else {
                dbCmd.CommandText = sbCmd.ToString();
            }            
        
            return dbCmd;
        }
    }
}
