﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.IoC;
using System.Data.Common;

namespace vJine.Core.ORM.Adapters {
    public partial class SQLite : IDbAdapter {

        static SQLite() {
            SQLite.Init_Keywords();
            SQLite.Init_TypeMap();
        }

        public SQLite() {
            this.Name = Class<SQLite>.Name;

            this.ParamPrefix = ":";
            this.GetDateTime = "Select datetime(CURRENT_TIMESTAMP,'localtime')";

            this.Quote_Open = "\"";
            this.Quote_Close = "\"";

            this.NULL = "";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = SQLite._KeyWords;
        }

        public override List<MapAttribute> GetMap<Tschema>() {
            return OrmConfig.Cache<SQLite, Tschema>.GetMap();
        }

        public override MapAttribute GetMap<Tschema>(Class<Tschema>.Property p) {
            return OrmConfig.Cache<SQLite, Tschema>.GetMap(p);
        }

        public override Exec<Tschema, DbCommand> Get_I<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<SQLite, Tschema>.Get_I(P);
        }

        public override Exec<DbDataReader, Tschema> Get_Q<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<SQLite, Tschema>.Get_Q(P);
        }

        public override List<Class<Tschema>.Property> GetFields<Tschema>() {
            return OrmConfig.Cache<SQLite, Tschema>.DbFields;
        }
    }
}
