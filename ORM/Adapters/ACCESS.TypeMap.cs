﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.IoC;
using System.Data;

namespace vJine.Core.ORM.Adapters {
    public partial class ACCESS : IDbAdapter {
        //http://www.cnblogs.com/wenjl520/archive/2009/03/02/1401575.html
        static void Init_TypeMap() {
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@bool, "bit", DbType.Boolean);
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@sbyte, "short", DbType.Int16, typeof(OrmTypeConverter.sbyte_short));
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@byte, "byte", DbType.Byte);
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@short, "short", DbType.Int16);
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@ushort, "integer", DbType.Int32, typeof(OrmTypeConverter.ushort_int));
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@int, "integer", DbType.Int32);
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@uint, "single", DbType.Single, typeof(OrmTypeConverter.uint_float));
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@long, "single", DbType.Single, typeof(OrmTypeConverter.long_float));
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@ulong, "single", DbType.Single, typeof(OrmTypeConverter.ulong_float));
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@float, "single", DbType.Single);
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@double, "double", DbType.Double);
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@decimal, "numeric", DbType.Decimal);

            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@char, "varchar(1)", DbType.AnsiString, typeof(OrmTypeConverter.char_string));
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@string, "varchar(50)", DbType.AnsiString);
            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@DateTime, "varchar(19)", DbType.AnsiString, typeof(OrmTypeConverter.DateTime_String));

            OrmConfig.Cache<ACCESS>.InitDefaultMap(Reflect.@byteArray, "LONGBINARY", DbType.Binary, typeof(OrmTypeConverter.bytes_));
        }
    }
}
