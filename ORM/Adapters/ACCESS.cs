﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace vJine.Core.ORM.Adapters {
    public partial class ACCESS : IDbAdapter {
        static ACCESS() {
            ACCESS.Init_TypeMap();
            ACCESS.Init_Keywords();
        }

        public ACCESS() {
            this.Name = Class<ACCESS>.Name;

            this.ParamPrefix = "@";
            this.GetDateTime = "Select now";

            this.Quote_Open = "[";
            this.Quote_Close = "]";

            this.NULL = " NULL";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = ACCESS._KeyWords;
        }

        public override List<Class<Tschema>.Property> GetFields<Tschema>() {
            return OrmConfig.Cache<ACCESS, Tschema>.DbFields;
        }

        public override List<MapAttribute> GetMap<Tschema>() {
            return OrmConfig.Cache<ACCESS, Tschema>.GetMap();
        }

        public override MapAttribute GetMap<Tschema>(Class<Tschema>.Property p) {
            return OrmConfig.Cache<ACCESS, Tschema>.GetMap(p);
        }

        public override Exec<Tschema, DbCommand> Get_I<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<ACCESS, Tschema>.Get_I(P);
        }

        public override Exec<DbDataReader, Tschema> Get_Q<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<ACCESS, Tschema>.Get_Q(P);
        }

        public override DbCommand PrepareDrop<Tdto>(bool IfExists, string table_name) {

            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;

            dbCmd.CommandText = "DROP TABLE " + table_name;

            return dbCmd;
        }
    }
}
