﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class PGSQL : IDbAdapter {
        //http://www.postgresql.org/docs/9.2/static/datatype.html
        static void Init_TypeMap() {
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@bool, "boolean", DbType.Boolean);
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@sbyte, "smallint", DbType.Int16, typeof(OrmTypeConverter.sbyte_short));
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@byte, "smallint", DbType.Int16, typeof(OrmTypeConverter.byte_short));
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@short, "smallint", DbType.Int16);
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@ushort, "integer", DbType.Int32, typeof(OrmTypeConverter.ushort_int));
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@int, "integer", DbType.Int32);
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@uint, "bigint", DbType.Int64, typeof(OrmTypeConverter.uint_long));
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@long, "bigint", DbType.Int64);
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@ulong, "real", DbType.Single, typeof(OrmTypeConverter.ulong_float));
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@float, "real", DbType.Single);
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@double, "double precision", DbType.Double);
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@decimal, "decimal", DbType.Decimal);

            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@char, "char(1)", DbType.AnsiStringFixedLength, typeof(OrmTypeConverter.char_string));
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@string, "varchar(50)", DbType.AnsiString);
            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@DateTime, "timestamp with time zone", DbType.DateTime);

            OrmConfig.Cache<PGSQL>.InitDefaultMap(Reflect.@byteArray, "bytea", DbType.Binary, typeof(OrmTypeConverter.bytes_));
        }
    }
}
