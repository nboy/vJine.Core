﻿using System;
using System.Collections.Generic;
using System.Text;
using vJine.Core.IoC;
using System.Data;

namespace vJine.Core.ORM.Adapters {
    public partial class MSSQL : IDbAdapter {
        // http://msdn.microsoft.com/en-us/library/cc716729.aspx
        static void Init_TypeMap() {

            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@bool, "BIT", DbType.Boolean);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@sbyte, "SMALLINT", DbType.Int16, typeof(OrmTypeConverter.sbyte_short));
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@byte, "TINYINT", DbType.Byte);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@short, "SMALLINT", DbType.Int16);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@ushort, "INT", DbType.Int32, typeof(OrmTypeConverter.ushort_int));
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@int, "INT", DbType.Int32);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@uint, "BIGINT", DbType.Int64, typeof(OrmTypeConverter.uint_long));
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@long, "BIGINT", DbType.Int64);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@ulong, "REAL", DbType.Single, typeof(OrmTypeConverter.ulong_float));
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@float, "REAL", DbType.Single);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@double, "FLOAT", DbType.Double);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@decimal, "DECIMAL", DbType.Decimal);

            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@char, "CHAR(1)", DbType.AnsiStringFixedLength, typeof(OrmTypeConverter.char_string));
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@string, "VARCHAR(50)", DbType.AnsiString);
            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@DateTime, "DATETIME", DbType.DateTime);

            OrmConfig.Cache<MSSQL>.InitDefaultMap(Reflect.@byteArray, "IMAGE", DbType.Binary, typeof(OrmTypeConverter.bytes_));
        }
    }
}
