﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class Oracle : IDbAdapter {
        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/yk72thhd.aspx
        /// http://docs.oracle.com/cd/B28359_01/appdev.111/b28395/oci03typ.htm#i429972
        /// </summary>
        static void Init_TypeMap() {

            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@bool, "CHAR(1)", DbType.AnsiStringFixedLength,  typeof(OrmTypeConverter.bool_string));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@sbyte, "NUMBER(3)", DbType.Int16, typeof(OrmTypeConverter.sbyte_short));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@byte, "NUMBER(3)", DbType.Int16, typeof(OrmTypeConverter.byte_short));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@short, "NUMBER(5)", DbType.Int32, typeof(OrmTypeConverter.short_int));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@ushort, "NUMBER(5)", DbType.Int32, typeof(OrmTypeConverter.ushort_int));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@int, "NUMBER(10)", DbType.Int64, typeof(OrmTypeConverter.int_long));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@uint, "NUMBER(10)", DbType.Int64, typeof(OrmTypeConverter.uint_long));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@long, "NUMBER(19)", DbType.Decimal, typeof(OrmTypeConverter.long_decimal));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@ulong, "NUMBER(19)", DbType.Decimal, typeof(OrmTypeConverter.ulong_decimal));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@float, "BINARY_FLOAT", DbType.Single);
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@double, "BINARY_DOUBLE", DbType.Double);
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@decimal, "DECIMAL(33,3)", DbType.Decimal);

            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@char, "CHAR(1)", DbType.AnsiStringFixedLength, typeof(OrmTypeConverter.char_string));
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@string, "VARCHAR2(50)", DbType.AnsiString);
            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@DateTime, "TIMESTAMP", DbType.DateTime/*, typeof(OrmTypeConverter.DateTime_)*/);

            OrmConfig.Cache<Oracle>.InitDefaultMap(Reflect.@byteArray, "BLOB", DbType.Binary, typeof(OrmTypeConverter.bytes_));
        }
    }
}
