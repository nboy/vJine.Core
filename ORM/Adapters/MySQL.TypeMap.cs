﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class MySQL : IDbAdapter {
        //http://dev.mysql.com/doc/refman/5.0/en/data-type-overview.html
        //http://www.cnblogs.com/bukudekong/archive/2011/06/27/2091590.html
        static void Init_TypeMap() {
            
            OrmConfig.Cache <MySQL>.InitDefaultMap(Reflect.@bool, "BOOL", DbType.Boolean);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@sbyte, "TINYINT", DbType.SByte, typeof(OrmTypeConverter.sbyte_));
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@byte, "TINYINT UNSIGNED", DbType.Byte);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@short, "SMALLINT", DbType.Int16);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@ushort, "SMALLINT UNSIGNED", DbType.Int16, typeof(OrmTypeConverter.ushort_));
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@int, "INT", DbType.Int32);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@uint, "INT UNSIGNED", DbType.UInt32, typeof(OrmTypeConverter.uint_));
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@long, "BIGINT", DbType.Int64);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@ulong, "BIGINT UNSIGNED", DbType.UInt64, typeof(OrmTypeConverter.ulong_));
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@float, "FLOAT", DbType.Single);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@double, "DOUBLE", DbType.Double);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@decimal, "DECIMAL", DbType.Decimal);

            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@char, "CHARACTER", DbType.AnsiStringFixedLength, typeof(OrmTypeConverter.char_string));
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@string, "VARCHAR(50)", DbType.AnsiString);
            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@DateTime, "DATETIME", DbType.DateTime);

            OrmConfig.Cache<MySQL>.InitDefaultMap(Reflect.@byteArray, "BLOB", DbType.Binary,  typeof(OrmTypeConverter.bytes_));
        }
    }
}
