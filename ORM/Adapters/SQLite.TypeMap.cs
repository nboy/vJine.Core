﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class SQLite : IDbAdapter {
        /// <summary>
        /// http://www.sqlite.org/datatype3.html
        /// http://system.data.sqlite.org/index.html/ci/ad2b42f3cc?sbs=0
        /// </summary>
        static void Init_TypeMap() {
            OrmConfig.Cache <SQLite>.InitDefaultMap(Reflect.@bool, "BIT", DbType.Boolean);
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@sbyte, "INT8", DbType.SByte, typeof(OrmTypeConverter.sbyte_));
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@byte, "UINT8", DbType.Byte);
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@short, "INT16", DbType.Int16);
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@ushort, "UINT16", DbType.UInt16, typeof(OrmTypeConverter.ushort_));
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@int, "INT32", DbType.Int32);
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@uint, "UINT32", DbType.UInt32, typeof(OrmTypeConverter.uint_));
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@long, "INT64", DbType.Int64);
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@ulong, "UINT64", DbType.UInt64, typeof(OrmTypeConverter.ulong_));
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@float, "DOUBLE", DbType.Double, typeof(OrmTypeConverter.float_double));
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@double, "DOUBLE", DbType.Double);
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@decimal, "DECIMAL", DbType.Decimal);

            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@char, "TEXT", DbType.AnsiStringFixedLength, typeof(OrmTypeConverter.char_string));
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@string, "TEXT", DbType.AnsiString);
            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@DateTime, "Text", DbType.AnsiString, typeof(OrmTypeConverter.DateTime_String));

            OrmConfig.Cache<SQLite>.InitDefaultMap(Reflect.@byteArray, "BLOB", DbType.Binary, typeof(OrmTypeConverter.bytes_));
        }
    }
}
