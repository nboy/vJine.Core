﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace vJine.Core.ORM.Adapters {
    public partial class PGSQL : IDbAdapter {
        static PGSQL() {
            PGSQL.Init_Keywords();
            PGSQL.Init_TypeMap();
        }

        public PGSQL() {
            this.Name = Class<PGSQL>.Name;

            this.ParamPrefix = "@";
            this.GetDateTime = "Select CURRENT_TIMESTAMP";

            this.Quote_Open = "\"";
            this.Quote_Close = "\"";

            this.NULL = " NULL";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = PGSQL._KeyWords;
        }

        public override List<Class<Tschema>.Property> GetFields<Tschema>() {
            return OrmConfig.Cache<PGSQL, Tschema>.DbFields;
        }

        public override List<MapAttribute> GetMap<Tschema>() {
            return OrmConfig.Cache<PGSQL, Tschema>.GetMap();
        }

        public override MapAttribute GetMap<Tschema>(Class<Tschema>.Property p) {
            return OrmConfig.Cache<PGSQL, Tschema>.GetMap(p);
        }

        public override Exec<Tschema, System.Data.Common.DbCommand> Get_I<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<PGSQL, Tschema>.Get_I(P);
        }

        public override Exec<System.Data.Common.DbDataReader, Tschema> Get_Q<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<PGSQL, Tschema>.Get_Q(P);
        }

        public override DbCommand PrepareQuery<Tdto>(int max, List<Class<Tdto>.Property> fields, string table_name, Class<Tdto>.Where where, List<Class<Tdto>.Property> orders) {
            int pCounter = 0;
            StringBuilder sbCmd = new StringBuilder();
            DbCommand dbCmd = this.Conn.CreateCommand();

            sbCmd.Append("Select");

            //Fields
            this.ToSelectString<Tdto>(dbCmd, sbCmd, fields);

            sbCmd.Append(" From ").Append(table_name);

            //Where
            if(where != null) {
                sbCmd.Append(" Where ");
                this.ToWhereString<Tdto>(
                    dbCmd, sbCmd, where, ref pCounter, false);
            }

            //Order By
            if(orders != null && orders.Count > 0) {
                sbCmd.Append(" Order By ");
                for(int j = 0, len_j = orders.Count - 1; j <= len_j; j++) {
                    Class<Tdto>.Property order_j = orders[j];
                    if(order_j.IsASC == null) {
                        continue;
                    }
                    sbCmd.Append(order_j.Name).Append(order_j.IsASC.Value ? " ASC" : " DESC");
                    if(j < len_j) {
                        sbCmd.Append(",");
                    }
                }
            }

            if(max > 0) {
                sbCmd.Append(" Limit ").Append(max.ToString());
            }

            dbCmd.CommandText = sbCmd.ToString();

            return dbCmd;
        }
    }
}
