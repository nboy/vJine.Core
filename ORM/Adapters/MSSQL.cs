﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using vJine.Core.IoC;

namespace vJine.Core.ORM.Adapters {
    public partial class MSSQL : IDbAdapter {

        static MSSQL() {
            MSSQL.Init_Keywords();
            MSSQL.Init_TypeMap();
        }

        public MSSQL() {
            this.Name = Class<MSSQL>.Name;

            this.ParamPrefix = "@";
            this.GetDateTime = "Select GETDATE()";

            this.Quote_Open = "[";
            this.Quote_Close = "]";

            this.NULL = " NULL";
            this.NOT_NULL = " NOT NULL";

            this.KeyWords = MSSQL._KeyWords;
        }

        public override List<Class<Tschema>.Property> GetFields<Tschema>() {
            return OrmConfig.Cache<MSSQL, Tschema>.DbFields;
        }

        public override List<MapAttribute> GetMap<Tschema>() {
            return OrmConfig.Cache<MSSQL, Tschema>.GetMap();
        }

        public override MapAttribute GetMap<Tschema>(Class<Tschema>.Property p) {
            return OrmConfig.Cache<MSSQL, Tschema>.GetMap(p);
        }

        public override Exec<Tschema, DbCommand> Get_I<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<MSSQL, Tschema>.Get_I(P);
        }

        public override Exec<DbDataReader, Tschema> Get_Q<Tschema>(IList<Class<Tschema>.Property> P) {
            return OrmConfig.Cache<MSSQL, Tschema>.Get_Q(P);
        }

        public override DbCommand PrepareDrop<Tdto>(bool IfExists, string table_name) {
            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;
            if (IfExists) {
                dbCmd.CommandText = 
                    "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'"+table_name +"') AND type in (N'U'))" + 
                    "DROP TABLE " + table_name;
            } else {
                dbCmd.CommandText = "DROP TABLE " + table_name;
            }

            return dbCmd;
        }
    }
}
