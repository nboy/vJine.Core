﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Configuration;

using vJine.Core.Base;
using vJine.Core.IoC;
using vJine.Core.IoC.Config;
using vJine.Core.IO;
using System.Reflection;
using vJine.Core.IO.Xml;
using System.Data.Common;
using System.Data;

namespace vJine.Core.ORM {
    [Serializable]
    [AppConfig("vJine.Net/OrmConfig")]
    public partial class OrmConfig {

        static OrmConfig() {
            try {
                OrmConfig my =
                    AppConfig<OrmConfig>.Get().Init();
                ;
                OrmConfig._get_My = () => {
                    return my;
                };
            } catch (Exception ex) {
                OrmConfig._get_My = () => {
                    throw new OrmException(ex, "Fail To Load vJine.Net/OrmConfig");
                };
            }
        }

        static Call<OrmConfig> _get_My = null;
        public static OrmConfig My {
            get {
                return _get_My();
            }
        }

        public static OrmConfig load(string orm_config_file) {
            return XmlHelper.Parse<OrmConfig>(orm_config_file).Init();
        }

        [XmlAttribute]
        public bool debug {
            get;
            set;
        }

        [XmlArray]
        public AdapterCollection Adapters { get; set; }

        [XmlArray]
        public dbCollection Connections { get; set; }

        [XmlArray]
        public ConverterCollection Converters { get; set; }

        [XmlArray]
        public TypeMapCollection TypeMaps { get; set; }

        [XmlArray]
        public SchemaCollection SchemaMaps { get; set; }
    }

    public partial class OrmConfig {

        [Serializable]
        public class Adapter {
            [XmlAttribute, ActiveFlag]
            public bool Active { get; set; }
            [XmlAttribute, Key(IsPrimary=true)]
            public string name { get; set; }
            [XmlAttribute]
            public string pattern { get; set; }
            [XmlElement]
            public Type type { get; set; }
        }

        [Serializable]
        public class AdapterCollection : Dictionary<string, Adapter> {
            [XmlAttribute, ActiveFlag]
            public bool Active { get; set; }
        }

        public class db {
            [XmlAttribute, Key(IsPrimary=true)]
            public string name { get; set; }
            [XmlAttribute]
            public string adapter { get; set; }

            [XmlAttribute]
            public string providerName { get; set; }

            [XmlElement]
            public string AssemblyQualifiedName { get; set; }
            [XmlElement]
            public string bin_path { get; set; }

            [XmlAttribute]
            public string host { get; set; } //{0}
            [XmlAttribute]
            public string port { get; set; } //{1}
            [XmlElement]
            public string user { get; set; } //{2}
            [XmlElement]
            public string password { get; set; } //{3}
            
            [XmlElement]
            public string connectionString { get; set; }

            internal Call<DbProviderFactory> get_Factory;
            internal Call<IDbAdapter> get_Adapter;
        }

        [Serializable]
        public class dbCollection : CollectionBase<db> {
            public db this[string name] {
                get {
                    for (int i = 0, len = this.Count; i < len; i++) {
                        if (base[i].name == name) {
                            return base[i];
                        }
                    }

                    return null;
                }
                set {
                    for (int i = 0, len = this.Count; i < len; i++) {
                        if (base[i].name == name) {
                            base[i] = value;
                        }
                    }
                }
            }
        }

        [Serializable]
        public class Converter {
            [XmlAttribute, Key(IsPrimary = true)]
            public string name { get; set; }
            [XmlAttribute]
            public Type type { get; set; }
        }

        [Serializable]
        public class ConverterCollection : Dictionary<string, Converter> {
            [ActiveFlag]
            [XmlAttribute]
            public bool Active { get; set; }
        }

        [Serializable]
        public class TypeMap {
            public TypeMap() {
                this.Active = true;
            }

            [XmlAttribute, ActiveFlag]
            public bool Active { get; set; }
            [XmlAttribute]
            public string db { get; set; }
            [XmlAttribute]
            public Type type { get; set; }
            [XmlAttribute]
            public string SQL_TYPE { get; set; }
            [XmlAttribute]
            public string Conv { get; set; }
        }

        [Serializable]
        public class TypeMapCollection : CollectionBase<TypeMap> {
            public TypeMapCollection() {
                this.Active = true;
            }

            [XmlAttribute, ActiveFlag]
            public bool Active { get; set; }
            [XmlAttribute]
            public string db { get; set; }
        }

        [Serializable]
        public class SchemaCollection : CollectionBase<Schema> {
            public SchemaCollection() {
                this.Active = true;
            }

            [XmlAttribute, ActiveFlag]
            public bool Active { get; set; }

            [XmlAttribute]
            public string adapter { get; set; }
        }

        [Serializable]
        public class Schema : CollectionBase<Map> {
            public Schema() {
                this.Active = true;
            }

            [XmlAttribute, ActiveFlag]
            public bool Active { get; set; }

            [XmlAttribute]
            public string adapter { get; set; }
            [XmlAttribute]
            public Type type { get; set; }
        }

        [Serializable]
        public class Map {

            [XmlAttribute, ActiveFlag]
            public bool Active { get; set; }

            [XmlAttribute]
            public string Name { get; set; }
            [XmlAttribute]
            public string Alias { get; set; }
            [XmlAttribute]
            public bool IsPrimary { get; set; }
            [XmlAttribute]
            public bool IsNullable { get; set; }
            [XmlAttribute]
            public string SQL_TYPE { get; set; }
            [XmlAttribute]
            public string Conv { get; set; }
            [XmlAttribute]
            DbType? DbType{ get; set;}
        }
    }
}
