﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using vJine.Core.IoC;
using System.Text.RegularExpressions;
using System.Data.Common;
using System.IO;
using System.Reflection;

namespace vJine.Core.ORM {
    public partial class OrmConfig {

        Dictionary<Type, Dictionary<Type, MapAttribute>> TypeMapIndex;
        Dictionary<Type, Dictionary<Type, Dictionary<string, MapAttribute>>> SchemaMapIndex;
        #region Init
        public OrmConfig Init() {
            this.Init_dbAdapters();
            this.Init_dbConfig();
            this.Init_Index();

            return this;
        }

        static string[] adapters =
                new string[] { 
                    "SQLite","SQLite",Class<vJine.Core.ORM.Adapters.SQLite>.AssemblyQualifiedName
                    ,"MySQL","MySQL",Class<vJine.Core.ORM.Adapters.MySQL>.AssemblyQualifiedName
                    ,"MSSQL","(MSSQL)|(SqlClient)",Class<vJine.Core.ORM.Adapters.MSSQL>.AssemblyQualifiedName
                    ,"Oracle","Oracle",Class<vJine.Core.ORM.Adapters.Oracle>.AssemblyQualifiedName
                    ,"PGSQL","(PGSQL)|(Npgsql)",Class<vJine.Core.ORM.Adapters.PGSQL>.AssemblyQualifiedName
                    ,"ACCESS","ACCESS",Class<vJine.Core.ORM.Adapters.ACCESS>.AssemblyQualifiedName
                };
        void Init_dbAdapters() {
            if (this.Adapters == null) {
                this.Adapters = new AdapterCollection();
            }

            for (int i = 0, len = OrmConfig.adapters.Length; i < len; i += 3) {
                string _name = OrmConfig.adapters[i];
                if (!this.Adapters.ContainsKey(_name)) {
                    this.Adapters.Add(_name,
                        new Adapter() {
                            Active = true, name = _name, 
                            pattern = OrmConfig.adapters[i + 1], 
                            type = Reflect.GetType(OrmConfig.adapters[i + 2], false)
                        });
                }
            }
        }

        void Init_dbConfig() {
            if (this.Connections == null) {
                this.Connections = new dbCollection();
            }

            ConnectionStringSettingsCollection cscc = ConfigurationManager.ConnectionStrings;
            for (int i = 0, len = cscc.Count; i < len; i++) {
                ConnectionStringSettings css_i = cscc[i];
                if (string.IsNullOrEmpty(css_i.Name)
                    || string.IsNullOrEmpty(css_i.ProviderName)
                    || string.IsNullOrEmpty(css_i.ConnectionString)) {
                    continue;
                }

                if (this.Connections[css_i.Name] == null) {
                    OrmConfig.db db_default = new OrmConfig.db() {
                        name = css_i.Name, providerName = css_i.ProviderName, connectionString = css_i.ConnectionString
                    };
                    this.Connections.Add(db_default);
                }
            }

            for (int i = 0, len = this.Connections.Count; i < len; i++) {
                this.get_dbAdapter(this.Connections[i]);
                this.get_dbFactory(this.Connections[i]);
            }
        }

        void get_dbAdapter(OrmConfig.db db_config) {
            Type T_adapter = null;
            if (!string.IsNullOrEmpty(db_config.adapter)) {
                T_adapter = this.get_dbAdapter(db_config.adapter);
                if (T_adapter == null) {
                    db_config.get_Adapter = () => {
                        throw new OrmException("No Suitable Adapter Found For [{0}]", db_config.name);
                    };
                } else {
                    db_config.get_Adapter = () => {
                        return Class.Create(T_adapter) as IDbAdapter;
                    };
                }
            } else {
                T_adapter = this.get_dbAdapter_by_pattern(db_config.providerName, db_config.name);
                if (T_adapter == null || !T_adapter.IsSubclassOf(typeof(IDbAdapter))) {
                    db_config.get_Adapter = () => {
                        throw new OrmException("No Suitable Adapter Found For [{0}]", db_config.name);
                    };
                } else {
                    db_config.get_Adapter = () => {
                        return Class.Create(T_adapter) as IDbAdapter;
                    };
                }
            }
        }

        public Type get_dbAdapter(string name_adapter) {
            if (string.IsNullOrEmpty(name_adapter)) {
                return null;
            }

            if (!this.Adapters.ContainsKey(name_adapter)) {
                return null;
            }

            return this.Adapters[name_adapter].type;
        }

        public Type get_dbAdapter_by_pattern(params string[] db_hints) {
            foreach (KeyValuePair<string, Adapter> kv in this.Adapters) {
                Adapter adapter = kv.Value;
                for (int i = 0, len = db_hints.Length; i < len; i++) {
                    if (Regex.IsMatch(db_hints[i], adapter.pattern, RegexOptions.IgnoreCase)) {
                        return adapter.type;
                    }
                }
            }

            return null;
        }

        void get_dbFactory(OrmConfig.db db_config) {
            try {
                DbProviderFactory db_factory = null;
                if (!string.IsNullOrEmpty(db_config.AssemblyQualifiedName)) {
                    Type T_factory = null;
                    if (db_config.bin_path != null && File.Exists(db_config.bin_path)) {
                        Assembly asmProvider =
                            Assembly.Load(File.ReadAllBytes(db_config.bin_path));
                        T_factory = asmProvider.GetType(db_config.AssemblyQualifiedName);
                    } else {
                        T_factory = Reflect.GetType(db_config.AssemblyQualifiedName);
                    }

                    FieldInfo fldInstance = 
                        T_factory.GetField("Instance", BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.Public);
                    if (fldInstance != null && fldInstance.FieldType.IsSubclassOf(typeof(DbProviderFactory))) {
                        object value = fldInstance.GetValue(null);
                        if (value != null) {
                            db_factory = value as DbProviderFactory;
                        }
                    }
                } else {
                    db_factory =
                        DbProviderFactories.GetFactory(db_config.providerName);
                }

                if (db_factory == null) {
                    db_config.get_Factory = () => {
                        throw new OrmException(
                        "获取数据库连接【{0}】【{1}】失败",
                        db_config.name, db_config.providerName);
                    };
                } else {
                    db_config.get_Factory = () => {
                        return db_factory;
                    };
                }
            } catch (Exception ex) {
                db_config.get_Factory = () => {
                    throw new OrmException(ex, 
                        "获取数据库连接【{0}】【{1}】失败", 
                        db_config.name, db_config.providerName);
                };
            }
        }

        void Init_Index() {
            this.Init_TypeMapIndex();
            this.Init_SchemaMapIndex();
        }

        /// <summary>
        /// 数据库-数据类型 映射
        /// </summary>
        void Init_TypeMapIndex() {
            if (this.TypeMapIndex != null) {
                return;
            }
            this.TypeMapIndex =
                    new Dictionary<Type, Dictionary<Type, MapAttribute>>();

            if (this.TypeMaps == null || !this.TypeMaps.Active || this.TypeMaps.Count == 0) {
                return;
            }

            for (int i = 0, len = this.TypeMaps.Count; i < len; i++) {
                TypeMap map_i = this.TypeMaps[i];
                if (string.IsNullOrEmpty(map_i.db) || string.IsNullOrEmpty(map_i.SQL_TYPE) || string.IsNullOrEmpty(map_i.Conv)) {
                    continue;
                }

                OrmException initEx = null;
                string db_name = string.IsNullOrEmpty(map_i.db) ? this.TypeMaps.db : map_i.db;
                Type db_adapter_type =
                    get_dbAdapter(string.IsNullOrEmpty(map_i.db) ? this.TypeMaps.db : map_i.db);
                if (db_adapter_type == null) {
                    initEx = new OrmException("db[{0}]adapter_type Is Null", db_name);
                }

                Type fieldType = map_i.type;
                Type converter = this.getConverter(map_i.Conv);
                if (converter == null && initEx == null) {
                    initEx = new OrmException("converter Is Null");
                }

                if (!this.TypeMapIndex.ContainsKey(db_adapter_type)) {
                    this.TypeMapIndex.Add(db_adapter_type, new Dictionary<Type, MapAttribute>());
                }
                Dictionary<Type, MapAttribute> dbMapIndex = this.TypeMapIndex[db_adapter_type];
                if (!dbMapIndex.ContainsKey(fieldType)) {
                    dbMapIndex.Add(fieldType,
                        new MapAttribute() { SQL_TYPE = map_i.SQL_TYPE, Conv = converter, initEx = initEx });
                } else {
                    dbMapIndex[fieldType] =
                        new MapAttribute() { SQL_TYPE = map_i.SQL_TYPE, Conv = converter, initEx = initEx };
                }
            }
        }

        //数据库-表-数据类型 映射
        void Init_SchemaMapIndex() {
            if (this.SchemaMapIndex != null) {
                return;
            }
            this.SchemaMapIndex =
                new Dictionary<Type, Dictionary<Type, Dictionary<string, MapAttribute>>>(); //Adapter,Class,Field,Map

            if (this.SchemaMaps == null || !this.SchemaMaps.Active || this.SchemaMaps.Count == 0) {
                return;
            }

            Dictionary<Type, Dictionary<Type, Dictionary<string, MapAttribute>>> schemaMapIndex = this.SchemaMapIndex;

            for (int i = 0, len_i = this.SchemaMaps.Count; i < len_i; i++) {
                Schema schemaMap_i = this.SchemaMaps[i];
                if (schemaMap_i == null || schemaMap_i.Count == 0) {
                    continue;
                }
                if (!schemaMap_i.Active || schemaMap_i.type == null) {
                    continue;
                }

                string db_adapter_name = 
                    string.IsNullOrEmpty(schemaMap_i.adapter) ? this.SchemaMaps.adapter : schemaMap_i.adapter;
                Type db_adapter_type = this.get_dbAdapter(db_adapter_name);
                if (db_adapter_type == null) {
                    continue;
                }

                if (!schemaMapIndex.ContainsKey(db_adapter_type)) {
                    schemaMapIndex.Add(db_adapter_type, new Dictionary<Type, Dictionary<string, MapAttribute>>());
                }
                //
                Dictionary<Type, Dictionary<string, MapAttribute>> dbMap_Index = schemaMapIndex[db_adapter_type];
                if (!dbMap_Index.ContainsKey(schemaMap_i.type)) {
                    dbMap_Index.Add(schemaMap_i.type, new Dictionary<string, MapAttribute>());
                }
                //
                Dictionary<string, MapAttribute> tbl_MapIndex = dbMap_Index[schemaMap_i.type];

                //
                for (int j = 0, len_j = schemaMap_i.Count; j < len_j; j++) {
                    Map map_j = schemaMap_i[j];
                    if (map_j == null || !map_j.Active) {
                        continue;
                    }

                    Type conv = null;
                    if (!string.IsNullOrEmpty(map_j.Conv) && this.Converters.ContainsKey(map_j.Conv)) {
                        conv = this.Converters[map_j.Conv].type;
                    }
                    MapAttribute map =
                        new MapAttribute() {
                            Name = map_j.Name, Alias = map_j.Alias, IsPrimary = map_j.IsPrimary, IsNullable = map_j.IsNullable, SQL_TYPE = map_j.SQL_TYPE,
                            Conv = conv,
                            initEx = conv != null ? null : new OrmException("Converter [{0},{1},{2}] Not Found.", schemaMap_i.adapter, Reflect.GetTypeName(schemaMap_i.type), map_j.Name)
                        };

                    if (tbl_MapIndex.ContainsKey(map_j.Name)) {
                        tbl_MapIndex[map_j.Name] = map;
                    } else {
                        tbl_MapIndex.Add(map_j.Name, map);
                    }
                }
            }
        }
        #endregion Init

        Type getConverter(string cvtKey) {
            if (!this.Converters.ContainsKey(cvtKey)) {
                return null;
            }

            return this.Converters[cvtKey].type;
        }

        MapAttribute getTypeMap<Tadapter, Tschema>(Class<Tschema>.Property p) where Tadapter : IDbAdapter {

            Type dbType = typeof(Tadapter);

            Dictionary<Type, Dictionary<Type, MapAttribute>> typeMapIndex = this.TypeMapIndex;
            if (!typeMapIndex.ContainsKey(dbType)) {
                return null;
            } else {
                Dictionary<Type, MapAttribute> baseMapIndex = typeMapIndex[dbType];
                if (!baseMapIndex.ContainsKey(p.pType)) {
                    return null;
                } else {
                    MapAttribute map = baseMapIndex[p.pType];
                    if (map.initEx != null) {
                        throw map.initEx;
                    }
                    return map;
                }
            }
        }

        MapAttribute GetSchemaMap<Tadapter, Tschema>(Class<Tschema>.Property p) where Tadapter : IDbAdapter {

            Type dbType = typeof(Tadapter);
            Type tblClass = typeof(Tschema);

            Dictionary<Type, Dictionary<Type, Dictionary<string, MapAttribute>>> dbMapIndex = this.SchemaMapIndex;
            if (!dbMapIndex.ContainsKey(dbType)) {
                return null;
            } else {
                Dictionary<Type, Dictionary<string, MapAttribute>> schemaMapIndex = dbMapIndex[dbType];
                if (!schemaMapIndex.ContainsKey(tblClass)) {
                    return null;
                } else {
                    Dictionary<string, MapAttribute> tbMapIndex = schemaMapIndex[tblClass];
                    if (!tbMapIndex.ContainsKey(p.Name)) {
                        return null;
                    } else {
                        return tbMapIndex[p.Name];
                    }
                }
            }
        }
    }
}
