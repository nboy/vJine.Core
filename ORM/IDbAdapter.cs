﻿using System.Collections.Generic;
using System.Data.Common;
using System.Text;

using vJine.Core.IoC;
using System;
using System.Reflection;

namespace vJine.Core.ORM {
    public abstract class IDbAdapter {
        static IDbAdapter() {
            Init_Priorities();
        }

        static Dictionary<string, int> Priorities = new Dictionary<string, int>();
        static void Init_Priorities() {
            Priorities.Add("*", 1); Priorities.Add("/", 1); Priorities.Add("%", 1);
            Priorities.Add("+", 2); Priorities.Add("-", 2);
            Priorities.Add(">", 3); Priorities.Add(">=", 3); Priorities.Add("<", 3); Priorities.Add("<=", 3);
            Priorities.Add("=", 4); Priorities.Add("!=", 4); //Priorities.Add("=?", 4);
            Priorities.Add("is", 4);
            Priorities.Add("like", 4); Priorities.Add("not like", 4);
            Priorities.Add("and", 5);
            Priorities.Add("or", 6);
        }

        public DbConnection Conn { get; set; }
        //public OrmConfig Config { get; set; }

        public IDbAdapter() {
            this.TYPE = this.GetType();
        }

        public string Name { get; protected set; }
        public Type TYPE { get; protected set; }

        public string ParamPrefix { get; protected set; }
        public string GetDateTime { get; protected set; }

        public string NULL { get; protected set; }
        public string NOT_NULL { get; protected set; }

        public string Quote_Open { get; set; }
        public string Quote_Close { get; set; }

        public List<string> KeyWords { get; protected set; }

        public abstract List<Class<Tschema>.Property> GetFields<Tschema>();
        //public abstract void GetFields<Tschema>(IList<Class<Tschema>.Property> map);
        public abstract List<MapAttribute> GetMap<Tschema>();
        public abstract MapAttribute GetMap<Tschema>(Class<Tschema>.Property p);
        public abstract Exec<Tschema,DbCommand> Get_I<Tschema>(IList<Class<Tschema>.Property> P);
        public abstract Exec<DbDataReader,Tschema> Get_Q<Tschema>(IList<Class<Tschema>.Property> P);

        public virtual DbCommand PrepareCreate<Tdto>(string table_name) {

            StringBuilder sbCreate =
                new StringBuilder("CREATE TABLE " + (table_name ?? Class<Tdto>.Name) + " (");

            Type t_dto = typeof(Tdto);
            List<Class<Tdto>.Property> P = this.GetFields<Tdto>();
            for (int i = 0, len_max = P.Count - 1; i <= len_max; i++) {
                Class<Tdto>.Property p_i = P[i];

                Type pType = p_i.pTypeNull != null ? p_i.pTypeNull : p_i.pType;

                MapAttribute map = this.GetMap<Tdto>(p_i);
                if (map == null) {
                    throw new OrmException("Type[{0}] Not Supprot For Create DDL", pType.FullName);
                }

                sbCreate.Append(map.Alias).Append(" ")
                    .Append(map.SQL_TYPE)
                    .Append((p_i.IsNullable || map.IsNullable) ? this.NULL : this.NOT_NULL);

                if (i < len_max) {
                    sbCreate.Append(",");
                }
            }

            //Primary Keys
            Class<Tdto>.Property[] Keys = Class<Tdto>.Keys;
            if (Keys.Length > 0) {
                sbCreate.Append(",CONSTRAINT PK_" + table_name + " PRIMARY KEY (");
                for (int i = 0, len_max = Keys.Length - 1; i <= len_max; i++) {
                    sbCreate.Append(this.GetMap<Tdto>(Keys[i]).Alias);
                    if (i < len_max) {
                        sbCreate.Append(",");
                    }
                }
                sbCreate.Append(")");
            }
            //Unique Keys
            Dictionary<string, List<Class<Tdto>.Property>> Uniques = Class<Tdto>.Uniques;
            foreach (KeyValuePair<string, List<Class<Tdto>.Property>> unique in Uniques) {
                sbCreate.Append(",CONSTRAINT UK_" + table_name + "_" + unique.Key + " UNIQUE (");
                List<Class<Tdto>.Property> _u_keys = unique.Value;
                for (int k = 0, len_k = _u_keys.Count - 1; k <= len_k; k++) {
                    Class<Tdto>.Property key_k = _u_keys[k];
                    sbCreate.Append(this.GetMap<Tdto>(key_k).Alias);
                    if (k < len_k) {
                        sbCreate.Append(",");
                    }
                }
                sbCreate.Append(")");
            }

            sbCreate.Append(")");

            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;
            dbCmd.CommandText = sbCreate.ToString();

            return dbCmd;
        }

        public virtual DbCommand PrepareDrop<Tdto>(bool IfExists, string table_name) {

            DbCommand dbCmd = this.Conn.CreateCommand();
            dbCmd.CommandType = System.Data.CommandType.Text;
            if (IfExists) {
                dbCmd.CommandText = "DROP TABLE IF EXISTS " + table_name;
            } else {
                dbCmd.CommandText = "DROP TABLE " + table_name;
            }

            return dbCmd;
        }

        public virtual void ToSelectString<Tdto>(DbCommand dbCmd, StringBuilder sbCmd, List<Class<Tdto>.Property> P) {
            sbCmd.Append(" ");
            for(int i = 0, len = P.Count - 1; i <= len; i++) {
                MapAttribute map_i = this.GetMap<Tdto>(P[i]);
                sbCmd.Append(map_i.name_alias);
                if(i < len) {
                    sbCmd.Append(",");
                }
            }
        }

        public DbCommand ToWhereString<Tdto>(string preCmd, Class<Tdto>.Where p) {
            DbCommand dbCmd = this.Conn.CreateCommand();
            StringBuilder sbCmd = new StringBuilder(preCmd);
            
            if (p != null) {
                sbCmd.Append(" Where ");
                int pCounter = 0;
                this.ToWhereString<Tdto>(dbCmd, sbCmd, p, ref pCounter, false);
            }

            dbCmd.CommandText = sbCmd.ToString();
            return dbCmd;
        }

        public virtual void ToWhereString<Tdto>(
            DbCommand dbCmd,
            StringBuilder sbCmd, Class<Tdto>.Property P, ref int pCounter, bool HasBrackets) {
            Class<Tdto>.Property L = P.L as Class<Tdto>.Property;
            Class<Tdto>.Property R = P.R as Class<Tdto>.Property;

            if (R == null && L == null) {
                return;
            }

            if (HasBrackets) {
                sbCmd.Append("(");
            }
            {
                if (L.Op == null) {
                    sbCmd.Append(this.GetMap<Tdto>(L).Alias); //*
                } else {
                    this.ToWhereString(
                        dbCmd,
                        sbCmd, L, ref pCounter, Priorities[P.Op] < Priorities[L.Op]);
                }

                sbCmd.Append(" ").Append(P.Op).Append(" ");

                if (R != null) {
                    if (R.Op == null) {
                        sbCmd.Append(this.GetMap<Tdto>(R).Alias); //*
                    } else {
                        this.ToWhereString(
                            dbCmd,
                            sbCmd, R, ref pCounter, Priorities[P.Op] < Priorities[R.Op]);
                    }
                } else {
                    if (P.Op == "is") {
                        sbCmd
                            .Append(P.R.ToString());
                    } else {
                        string ParamName =
                                this.ParamPrefix + "P_" + pCounter++.ToString();
                        sbCmd.Append(ParamName);

                        if (dbCmd != null) {
                            DbParameter param = dbCmd.CreateParameter();
                            param.ParameterName = ParamName;

                            MapAttribute map = this.GetMap<Tdto>(L);
                            param.Value = 
                                map.Conv_I != null ? map.Conv_I.Invoke(null, new object[] { P.R }) : param.Value = P.R;

                            dbCmd.Parameters.Add(param);
                        }
                    }
                }
            }
            if (HasBrackets) {
                sbCmd.Append(")");
            }
        }

        public virtual void ToUpdateString<Tdto>(DbCommand dbCmd, StringBuilder sbCmd, Class<Tdto>.Property P, ref int pCounter) {
            Class<Tdto>.Property L = P.L as Class<Tdto>.Property;
            Class<Tdto>.Property R = P.R as Class<Tdto>.Property;

            if (R == null && L == null) {
                sbCmd.Append(P.Name);
            } else {
                if (L.Op == null) {
                    sbCmd.Append(this.GetMap<Tdto>(L).Alias); //*
                } else {
                    this.ToUpdateString(dbCmd, sbCmd, L, ref pCounter);
                }

                sbCmd.Append(P.Op).Append(" ");

                if (R != null) {
                    if (R.Op == null) {
                        sbCmd.Append(this.GetMap<Tdto>(R).Alias); //*
                    } else {
                        this.ToUpdateString<Tdto>(dbCmd, sbCmd, R, ref pCounter);
                    }
                } else {
                    string ParamName = this.ParamPrefix + "U_" + pCounter++.ToString();
                    sbCmd.Append(ParamName);

                    if (dbCmd != null) {
                        DbParameter param = dbCmd.CreateParameter();
                        param.ParameterName = ParamName;
                        MapAttribute map = this.GetMap<Tdto>(L);
                        param.Value =
                                map.Conv_I != null ? map.Conv_I.Invoke(null, new object[] { P.R }) : param.Value = P.R;

                        if (param.Value == null) {
                            param.Value = DBNull.Value;
                        }

                        dbCmd.Parameters.Add(param);
                    }
                }
            }
        }

        public virtual DbCommand PrepareInsert<Tdto>(string table_name, IList<Class<Tdto>.Property> Keys) {
            StringBuilder sbCmd = new StringBuilder();
            DbCommand dbCmd = this.Conn.CreateCommand();

            for (int j = 0, len_j = Keys.Count - 1; j <= len_j; j++) {
                MapAttribute map_j = this.GetMap<Tdto>(Keys[j]);
                DbParameter dbParam = dbCmd.CreateParameter();
                dbParam.ParameterName = this.ParamPrefix + map_j.Alias; //*
                dbParam.DbType = map_j.DbType.Value;
                dbCmd.Parameters.Add(dbParam);
            }

            DbParameterCollection db_params = dbCmd.Parameters;
            if(string.IsNullOrEmpty(table_name)) {
                sbCmd.Append("Insert Into {0} (");
            } else {
                sbCmd.Append("Insert Into ").Append(table_name).Append(" (");
            }
            for(int j = 0, len_j = Keys.Count - 1; j <= len_j; j++) {
                sbCmd.Append(this.GetMap<Tdto>(Keys[j]).Alias); //*

                if(j < len_j) {
                    sbCmd.Append(",");
                }
            }
            sbCmd.Append(") Values (");
            for(int j = 0, len_j = db_params.Count - 1; j <= len_j; j++) {
                sbCmd.Append(db_params[j].ParameterName);

                if(j < len_j) {
                    sbCmd.Append(",");
                }
            }
            sbCmd.Append(")");

            dbCmd.CommandText = sbCmd.ToString();
            return dbCmd;
        }

        public virtual DbCommand PrepareDelete<Tdto>(string table_name, Class<Tdto>.Where Where) {
            StringBuilder sbCmd = new StringBuilder();
            DbCommand dbCmd = this.Conn.CreateCommand();
            int pCounter = 0;

            sbCmd.Append("Delete From ").Append(table_name);

            if(Where != null) {
                sbCmd.Append(" Where ");
                this.ToWhereString<Tdto>(
                    dbCmd, sbCmd, Where, ref pCounter, false);
            }

            dbCmd.CommandText = sbCmd.ToString();
            return dbCmd;
        }

        public virtual DbCommand PrepareUpdate<Tdto>(string table_name, Class<Tdto>.Set Set, Class<Tdto>.Where Where) {
            StringBuilder sbCmd = new StringBuilder();

            int pCounter = 0;
            DbCommand dbCmd = this.Conn.CreateCommand();

            sbCmd.Append("Update ").Append(table_name).Append(" Set ");
            //Set
            this.ToUpdateString<Tdto>(dbCmd, sbCmd, Set, ref pCounter);

            //Where
            if(Where != null) {
                sbCmd.Append(" Where ");
                this.ToWhereString<Tdto>(
                    dbCmd, sbCmd, Where, ref pCounter, false);
            }

            dbCmd.CommandText = sbCmd.ToString();
            return dbCmd;
        }

        public virtual DbCommand PrepareQuery<Tdto>(int max, List<Class<Tdto>.Property> fields, string table_name, Class<Tdto>.Where where, List<Class<Tdto>.Property> orders) {
            int pCounter = 0;
            StringBuilder sbCmd = new StringBuilder();
            DbCommand dbCmd = this.Conn.CreateCommand();

            sbCmd.Append("Select");

            if(max > 0) {
                sbCmd.Append(" Top ").Append(max).Append(" ");
            }

            //Fields
            this.ToSelectString<Tdto>(dbCmd, sbCmd, fields);

            sbCmd.Append(" From ").Append(table_name);

            //Where
            if(where != null) {
                sbCmd.Append(" Where ");
                this.ToWhereString<Tdto>(
                    dbCmd, sbCmd, where, ref pCounter, false);
            }

            //Order By
            if(orders != null && orders.Count > 0) {
                sbCmd.Append(" Order By ");
                for(int j = 0, len_j = orders.Count - 1; j <= len_j; j++) {
                    Class<Tdto>.Property order_j = orders[j];
                    if(order_j.IsASC == null) {
                        continue;
                    }
                    sbCmd.Append(order_j.Name).Append(order_j.IsASC.Value ? " ASC" : " DESC");
                    if(j < len_j) {
                        sbCmd.Append(",");
                    }
                }
            }

            dbCmd.CommandText = sbCmd.ToString();

            return dbCmd;
        }
    }
}