﻿using System;
using vJine.Core.IoC;
using System.Text;
using System.Threading;

namespace vJine.Core.IO {
    public class DataBuffer<T> {
        public DataBuffer()
            : this(4096) {
        }

        public DataBuffer(int capacity)
            : this(capacity, null) {
        }

        public DataBuffer(int capacity, Exec<T[], int> on_full) {
            this.Capacity = capacity;
            this.Data = new T[capacity];

            if (on_full != null) {
                this.OnFull += on_full;
            }
        }

        T[] Data = null;
        int Capacity = 0, lenBuff = 0;
        public event Exec<T[], int> OnFull;

        public void Write(T[] Data) {
            this.Write(Data, 0, Data.Length);
        }

        public void Write(T[] Data, int len) {
            this.Write(Data, 0, len);
        }

        public void Write(T[] data, int start, int len) {
            lock (this) {
                int len_Write = this.lenBuff + len;
                int times = len_Write / this.Capacity;
                if (len_Write % this.Capacity > 0) {
                    times += 1;
                }

                int len_write = 0; bool HasMoreData = false;
                for (int i = 0; i < times; i++) {
                    len_write = this.Capacity - this.lenBuff;

                    HasMoreData = len > len_write;
                    len_write = HasMoreData ? len_write : len;

                    Array.Copy(data, start, this.Data, this.lenBuff, len_write);
                    start += len_write; len -= len_write; this.lenBuff += len_write;

                    if (HasMoreData || len_write == this.Capacity) {
                        this.OnFull(this.Data, this.lenBuff);
                        this.lenBuff = 0;
                    }
                }
            }
        }

        public void Flush() {
            lock (this) {
                if (this.lenBuff > 0) {
                    this.OnFull(this.Data, this.lenBuff);
                    this.lenBuff = 0;
                }
            }
        }
    }
}
