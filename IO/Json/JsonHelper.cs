﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using vJine.Core.IoC;
using System.Reflection;
using System.Collections;
using vJine.Core.ORM;

namespace vJine.Core.IO.Json {
    public partial class JsonHelper {
        #region ToString

        public static string ToString(object jsonObject) {
            MemoryStream mm = new MemoryStream();
            JsonHelperCache.ToString(jsonObject.GetType())(jsonObject, mm);

            string json = Encoding.UTF8.GetString(mm.ToArray());
            mm.Close(); mm = null;
            return json;
        }

        public static void ToString(object jsonObject, Stream jsonStream) {
            JsonHelperCache.ToString(jsonObject.GetType())(jsonObject, jsonStream);
        }

        public static string ToString<T>(T jsonObject) {
            return JsonHelper<T>.ToString(jsonObject);
        }

        public static void ToString<T>(T jsonObject, Stream jsonStream) {
            JsonHelper<T>.ToString(jsonObject, jsonStream);
        }

        #endregion ToString

        #region Parse

        public static void Parse(string json, object jsonObject) {
            JsonHelper.Parse(Encoding.UTF8.GetBytes(json), jsonObject);
        }

        public static void Parse(byte[] json, object jsonObject) {
            MemoryStream jsonStream = new MemoryStream(json);
            jsonStream.Position = 0;

            parseObject.MakeGenericMethod(jsonObject.GetType())
                .Invoke(null, new object[] { jsonStream, new byte[6], jsonObject });
        }

        public static object Parse(string json, Type objType) {
            return
                parseString.MakeGenericMethod(objType)
                .Invoke(null, new object[] { json });
        }

        public static object Parse(byte[] json, Type objType) {
            return
                parseBytes.MakeGenericMethod(objType)
                .Invoke(null, new object[] { json });
        }

        static MethodInfo parseString =
                (new Call<JsonHelper, string>(Parse<JsonHelper>)).Method.GetGenericMethodDefinition();
        public static T Parse<T>(string json) {
            return Parse<T>(Encoding.UTF8.GetBytes(json));
        }

        static MethodInfo parseBytes =
            new Call<JsonHelper, byte[]>(Parse<JsonHelper>).Method.GetGenericMethodDefinition();
        public static T Parse<T>(byte[] json) {
            MemoryStream jsonStream =
                new MemoryStream(json);
            jsonStream.Position = 0;

            T jsonObject = Class<T>.Create();

            Parse<T>(jsonStream, new byte[6], jsonObject);

            return jsonObject;
        }

        public static T Parse<T>(Stream jsonStream) {
            T jsonObject = Class<T>.Create();

            Parse<T>(jsonStream, new byte[6], jsonObject);

            return jsonObject;
        }

        public static T Parse<T>(Stream jsonStream, T jsonObject) {
            Parse<T>(jsonStream, new byte[6], jsonObject);

            return jsonObject;
        }
        #endregion Parse
    }
}
