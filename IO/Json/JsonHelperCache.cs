﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using vJine.Core.IoC;

namespace vJine.Core.IO.Json {
    public class JsonHelperCache {
        static JsonHelperCache() {
            JsonHelperCache.ToString(Reflect.@bool);
            JsonHelperCache.ToString(Reflect.@sbyte);
            JsonHelperCache.ToString(Reflect.@byte);
            JsonHelperCache.ToString(Reflect.@char);
            JsonHelperCache.ToString(Reflect.@short);
            JsonHelperCache.ToString(Reflect.@ushort);
            JsonHelperCache.ToString(Reflect.@int);
            JsonHelperCache.ToString(Reflect.@uint);
            JsonHelperCache.ToString(Reflect.@long);
            JsonHelperCache.ToString(Reflect.@ulong);
            JsonHelperCache.ToString(Reflect.@float);
            JsonHelperCache.ToString(Reflect.@double);
            JsonHelperCache.ToString(Reflect.@decimal);
            JsonHelperCache.ToString(Reflect.@DateTime);
            JsonHelperCache.ToString(Reflect.@string);
        }

        static Dictionary<Type, Exec<object, Stream>> ToStringCache = new Dictionary<Type, Exec<object, Stream>>();
        static Type TjsonHelper = typeof(JsonHelper<string>).GetGenericTypeDefinition();
        public static Exec<object, Stream> ToString(Type Tjson) {
            lock (ToStringCache) {
                if (ToStringCache.ContainsKey(Tjson)) {
                    return ToStringCache[Tjson];
                }

                MethodInfo obj_toString =
                    TjsonHelper.MakeGenericType(Tjson).GetMethod("ToString", new Type[] { Tjson, Reflect.@Stream });

                DynamicMethod dm =
                    new DynamicMethod("", Reflect.@void, new Type[] { Reflect.@object, Reflect.@Stream });
                ILGenerator ilGen = dm.GetILGenerator();

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Unbox_Any, Tjson);
                ilGen.Emit(OpCodes.Ldarg_1);
                Emit.Opc_Call(ilGen, obj_toString);
                ilGen.Emit(OpCodes.Ret);

                Exec<object, Stream> helper =
                    dm.CreateDelegate(typeof(Exec<object, Stream>)) as Exec<object, Stream>;
                ToStringCache.Add(Tjson, helper);

                return helper;
            }
        }

        static Dictionary<Type, Call<object, Stream>> ParseCache = new Dictionary<Type, Call<object, Stream>>();
        public static Call<object, Stream> Parse(Type Tjson) {
            lock (ParseCache) {
                if (ParseCache.ContainsKey(Tjson)) {
                    return ParseCache[Tjson];
                }

                MethodInfo obj_parse =
                    TjsonHelper.MakeGenericType(Tjson).GetMethod("Parse", new Type[] { Reflect.@Stream });

                DynamicMethod dm = new DynamicMethod("", Reflect.@object, new Type[] { Reflect.@Stream });
                ILGenerator ilGen = dm.GetILGenerator();

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, obj_parse);
                ilGen.Emit(OpCodes.Box, Tjson);
                ilGen.Emit(OpCodes.Ret);

                Call<object, Stream> helper =
                    dm.CreateDelegate(typeof(Call<object, Stream>)) as Call<object, Stream>;
                ParseCache.Add(Tjson, helper);

                return helper;
            }
        }
    }
}
