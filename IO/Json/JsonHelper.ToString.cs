﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

using vJine.Core.Base;
using vJine.Core.IoC;
using vJine.Core.ORM;

namespace vJine.Core.IO.Json {

    public partial class JsonHelper<T> {

        public static string ToString(T jsonObject) {
            MemoryStream mm = new MemoryStream();
            JsonHelper<T>.helper(jsonObject, mm);
            return Encoding.UTF8.GetString(mm.ToArray());
        }

        public static void ToString(T jsonObject, string json_file) {
            FileStream jsonStream =
                new FileStream(json_file, FileMode.OpenOrCreate | FileMode.Truncate);

            JsonHelper<T>.helper(jsonObject, jsonStream);

            jsonStream.Close();
        }

        public static void ToString(T jsonObject, Stream json_stream) {
            JsonHelper<T>.helper(jsonObject, json_stream);
        }
    }

    public partial class JsonHelper<T> {
        static MethodInfo string_toLower = Reflect.@string.GetMethod("ToLower", Type.EmptyTypes);
        static Type T_nullable = typeof(Nullable<int>).GetGenericTypeDefinition();
        static readonly Type Tobj = typeof(T);

        static Exec<T, Stream> helper = null;
        static JsonHelper() {
            try {
                JsonHelper<T>.helper = JsonHelper<T>.Create();
            } catch (Exception ex) {
                JsonHelper<T>.helper = (T Tjson, Stream jsonStream) => {
                    throw ex;
                };
            }
        }

        public static Exec<T, Stream> Create() {
            return JsonHelper<T>.Create(Class<T>.GetMap());
        }

        public static Exec<T, Stream> Create(params Class<T>.Property[] P) {
            DynamicMethod dmToString =
                new DynamicMethod("", Reflect.@void, new Type[] { JsonHelper<T>.Tobj, Reflect.@Stream }, true);
            Dictionary<Type, LocalBuilder> lvs = new Dictionary<Type, LocalBuilder>();

            ILGenerator ilGen = dmToString.GetILGenerator();
            Call<LocalBuilder, Type> getVariable = (Type T_var) => {
                if (lvs.ContainsKey(T_var)) {
                    return lvs[T_var];
                }

                LocalBuilder lv = ilGen.DeclareLocal(T_var);
                lvs.Add(T_var, lv);

                return lv;
            };
            Exec<Type, string> gen_toString = (Type T_obj, string format) => {
                if (!string.IsNullOrEmpty(format)) {
                    ilGen.Emit(OpCodes.Ldstr, format);
                    ilGen.Emit(OpCodes.Call,
                        T_obj.GetMethod("ToString", new Type[] { typeof(string) }));
                } else {
                    ilGen.Emit(OpCodes.Call,
                        T_obj.GetMethod("ToString", Type.EmptyTypes));
                }
            };
            Exec<bool> gen_jsonWrite = (bool IsConvert) => {
                //json_write(string,stream,bool);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.Emit(IsConvert ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
                ilGen.Emit(OpCodes.Call, json_Write);
            };

            #region gen_properties

            bool HasProperties = false;
            Exec _genPropertyToString = () => {
                for (int i = 0, len = P.Length; i < len; i++) {
                    Class<T>.Property p_i = P[i];
                    if (p_i.IsXmlIgnore) {
                        continue;
                    }
                    //Key
                    ilGen.Emit(OpCodes.Ldstr, (HasProperties ? "," : "") + "\"" + p_i.Name + "\":");
                    gen_jsonWrite(false);
                    //Value
                    Label lbNextProperty = ilGen.DefineLabel();
                    //get_value;
                    if (p_i.IsNullable) {
                        Label lbHasValue = ilGen.DefineLabel();
                        LocalBuilder lvNull = getVariable(p_i.pType);
                        Type Tnull = T_nullable.MakeGenericType(p_i.pTypeNull);

                        ilGen.Emit(OpCodes.Ldarg_0);
                        Emit.Opc_Call(ilGen, p_i.get);
                        ilGen.Emit(OpCodes.Stloc, lvNull);
                        ilGen.Emit(OpCodes.Ldloca, lvNull);
                        ilGen.Emit(OpCodes.Call, Tnull.GetProperty("HasValue").GetGetMethod());
                        ilGen.Emit(OpCodes.Brtrue, lbHasValue);

                        ilGen.Emit(OpCodes.Ldstr, "null");
                        gen_jsonWrite(false);
                        ilGen.Emit(OpCodes.Br, lbNextProperty);

                        ilGen.MarkLabel(lbHasValue);
                        ilGen.Emit(OpCodes.Ldloca, lvNull);
                        ilGen.Emit(OpCodes.Call, Tnull.GetProperty("Value").GetGetMethod());
                    } else {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        Emit.Opc_Call(ilGen, p_i.get);
                    }

                    Type p_type = p_i.IsNullable ? p_i.pTypeNull : p_i.pType;
                    if (p_i.IsBool) {
                        LocalBuilder lv = getVariable(p_type);
                        ilGen.Emit(OpCodes.Stloc, lv);
                        ilGen.Emit(OpCodes.Ldloca, lv);

                        gen_toString(p_type, "");
                        Emit.Opc_Call(ilGen, string_toLower);

                        gen_jsonWrite(false);
                    } else if (p_i.IsNumber) {
                        LocalBuilder lv = getVariable(p_type);
                        ilGen.Emit(OpCodes.Stloc, lv);
                        ilGen.Emit(OpCodes.Ldloca, lv);

                        gen_toString(p_type, "");
                        gen_jsonWrite(false);
                    } else if (p_i.IsChar) {
                        LocalBuilder lv = getVariable(p_type);
                        ilGen.Emit(OpCodes.Stloc, lv);
                        ilGen.Emit(OpCodes.Ldloca, lv);

                        gen_toString(p_type, "");
                        gen_jsonWrite(true);
                    } else if (p_i.IsString) {

                        gen_jsonWrite(true);
                    } else if (p_i.IsDateTime) {
                        LocalBuilder lv = getVariable(p_type);
                        ilGen.Emit(OpCodes.Stloc, lv);
                        ilGen.Emit(OpCodes.Ldloca, lv);

                        gen_toString(p_type, "r");

                        gen_jsonWrite(true);
                    } else if (p_i.IsEnum) {
                        ilGen.Emit(OpCodes.Box, p_type);
                        ilGen.Emit(OpCodes.Callvirt, typeof(object).GetMethod("ToString", Type.EmptyTypes));
                        gen_jsonWrite(true);
                    } else {
                        MethodInfo json_tostring =
                            Tthis.MakeGenericType(p_type).GetMethod("ToString", new Type[] { p_type, Reflect.@Stream });

                        ilGen.Emit(OpCodes.Ldarg_1);
                        Emit.Opc_Call(ilGen, json_tostring);
                    }
                    ilGen.MarkLabel(lbNextProperty);

                    HasProperties = true;
                }
            };
            #endregion gen_properties

            if (JsonHelper<T>.Tobj == Reflect.@object) {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldarg_1);
                Emit.Opc_Call(ilGen, object_toString);
            } else if (Reflect.IsBool(JsonHelper<T>.Tobj)) {
                ilGen.Emit(OpCodes.Ldarg_0);
                LocalBuilder lv = getVariable(JsonHelper<T>.Tobj);
                ilGen.Emit(OpCodes.Stloc, lv);
                ilGen.Emit(OpCodes.Ldloca, lv);
                gen_toString(JsonHelper<T>.Tobj, "");
                Emit.Opc_Call(ilGen, string_toLower);
                gen_jsonWrite(false);
            } else if (Reflect.IsNumber(JsonHelper<T>.Tobj)) {
                ilGen.Emit(OpCodes.Ldarg_0);
                LocalBuilder lv = getVariable(JsonHelper<T>.Tobj);
                ilGen.Emit(OpCodes.Stloc, lv);
                ilGen.Emit(OpCodes.Ldloca, lv);

                gen_toString(JsonHelper<T>.Tobj, "");
                gen_jsonWrite(false);
            } else if (Reflect.IsChar(JsonHelper<T>.Tobj)) {
                ilGen.Emit(OpCodes.Ldarg_0);
                LocalBuilder lv = getVariable(JsonHelper<T>.Tobj);
                ilGen.Emit(OpCodes.Stloc, lv);
                ilGen.Emit(OpCodes.Ldloca, lv);

                gen_toString(JsonHelper<T>.Tobj, "");
                gen_jsonWrite(true);
            } else if (JsonHelper<T>.Tobj == Reflect.@string) {
                ilGen.Emit(OpCodes.Ldarg_0);
                gen_jsonWrite(true);
            } else if (Reflect.IsDateTime(JsonHelper<T>.Tobj)) {
                ilGen.Emit(OpCodes.Ldarg_0);
                gen_toString(JsonHelper<T>.Tobj, "");
                gen_jsonWrite(true);
            } else if (Reflect.IsEnum(JsonHelper<T>.Tobj)) {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Box, JsonHelper<T>.Tobj);
                ilGen.Emit(OpCodes.Callvirt, typeof(object).GetMethod("ToString", Type.EmptyTypes));
                gen_jsonWrite(true);
            } else {
                Label lbIsNotNull = ilGen.DefineLabel();
                {//if(objT == null) return "null";
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Ldnull);
                    ilGen.Emit(OpCodes.Ceq);

                    ilGen.Emit(OpCodes.Brfalse, lbIsNotNull);
                    {
                        ilGen.Emit(OpCodes.Ldstr, "null");
                        gen_jsonWrite(false);
                        ilGen.Emit(OpCodes.Ret);
                    }
                } // else {
                {
                    ilGen.MarkLabel(lbIsNotNull);
                    ilGen.Emit(OpCodes.Ldstr, "{");
                    gen_jsonWrite(false);

                    _genPropertyToString();
                }

                if (Reflect.IsArray(JsonHelper<T>.Tobj)) {
                    ilGen.Emit(OpCodes.Ldstr, HasProperties ? ",Items:" : "Items:");
                    gen_jsonWrite(false);

                    MethodInfo list_tostring =
                                list_toString.MakeGenericMethod(JsonHelper<T>.Tobj.GetElementType());

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Ldarg_1);
                    Emit.Opc_Call(ilGen, list_tostring);
                } else if (Reflect.IsList(JsonHelper<T>.Tobj)) {
                    ilGen.Emit(OpCodes.Ldstr, HasProperties ? ",Items:" : "Items:");
                    gen_jsonWrite(false);

                    MethodInfo list_tostring =
                                list_toString.MakeGenericMethod(Reflect.GetGenericArgs(JsonHelper<T>.Tobj));

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Ldarg_1);
                    Emit.Opc_Call(ilGen, list_tostring);
                } else if (Reflect.IsDictionary(JsonHelper<T>.Tobj)) {
                    ilGen.Emit(OpCodes.Ldstr, HasProperties ? ",Items:" : "Items:");
                    gen_jsonWrite(false);

                    MethodInfo dict_tostring =
                                dict_toString.MakeGenericMethod(Reflect.GetGenericArgs(JsonHelper<T>.Tobj));

                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Ldarg_1);
                    Emit.Opc_Call(ilGen, dict_tostring);
                }

                ilGen.Emit(OpCodes.Ldstr, "}");
                gen_jsonWrite(false);
            }

            ilGen.Emit(OpCodes.Ret);
            return dmToString.CreateDelegate(typeof(Exec<T, Stream>)) as Exec<T, Stream>;
        }

        static Type Tthis = typeof(JsonHelper<T>).GetGenericTypeDefinition();
        static MethodInfo object_toString = new Exec<object, Stream>(ToString).Method;
        static void ToString(object jsonObject, Stream jsonStream) {
            if (jsonObject == null) {
                jsonWrite("null", jsonStream, false);
                return;
            }

            JsonHelperCache.ToString(jsonObject.GetType())(jsonObject, jsonStream);
        }

        static MethodInfo list_toString =
            new Exec<IList<JsonHelper<T>>, Stream>(ToString).Method.GetGenericMethodDefinition();
        static void ToString<v>(IList<v> list, Stream jsonStream) {
            if (list == null) {
                jsonWrite("null", jsonStream, false);
                return;
            }
            Exec<v, Stream> list_helper = JsonHelper<v>.helper;

            jsonWrite("[", jsonStream, false);
            for (int i = 0, len = list.Count; i < len; i++) {
                if (i > 0) {
                    jsonWrite(",", jsonStream, false);
                }

                list_helper(list[i], jsonStream);
            }
            jsonWrite("]", jsonStream, false);
        }

        static MethodInfo dict_toString =
            new Exec<Dictionary<JsonHelper<T>, JsonHelper<T>>, Stream>(ToString).Method.GetGenericMethodDefinition();
        static void ToString<k, v>(Dictionary<k, v> dict, Stream jsonStream) {

            if (dict == null) {
                jsonWrite("null", jsonStream, false);
                return;
            }
            Exec<k, Stream> key_helper = JsonHelper<k>.helper;
            Exec<v, Stream> value_helper = JsonHelper<v>.helper;

            int index = 0, len = dict.Keys.Count;
            jsonWrite("[", jsonStream, false);
            foreach (KeyValuePair<k, v> kv in dict) {
                if (index > 0) {
                    jsonWrite(",", jsonStream, false);
                }

                key_helper(kv.Key, jsonStream);
                jsonWrite(",", jsonStream, false);
                value_helper(kv.Value, jsonStream);

                index += 1;
            }
            jsonWrite("]", jsonStream, false);
        }

        static Encoding utf_8 = Encoding.UTF8;
        static byte[] B_NULL = Encoding.UTF8.GetBytes("null");
        static MethodInfo json_Write = new Exec<string, Stream, bool>(jsonWrite).Method;
        static void jsonWrite(string v, Stream jsonStream, bool Is_convert) {
            if (v == null) {
                jsonStream.Write(B_NULL, 0, B_NULL.Length);
                return;
            }

            //Encoding utf_8 = Encoding.UTF8;
            if (!Is_convert) {
                byte[] B = utf_8.GetBytes(v);
                jsonStream.Write(B, 0, B.Length);
                return;
            } else {
                byte[] B = utf_8.GetBytes(convert(v));
                jsonStream.Write(B, 0, B.Length);
            }
        }

        static string convert(string v) {
            StringBuilder json_string = new StringBuilder();
            json_string.Append("\"");
            for (int i = 0, len = v.Length; i < len; i++) {
                char c = v[i];
                switch (c) {
                    case '\"':
                        json_string.Append("\\\""); continue;
                    case '\\':
                        json_string.Append("\\\\"); continue;
                    case '/':
                        json_string.Append("\\/"); continue;
                    case '\b':
                        json_string.Append("\\b"); continue;
                    case '\f':
                        json_string.Append("\\f"); continue;
                    case '\n':
                        json_string.Append("\\n"); continue;
                    case '\r':
                        json_string.Append("\\r"); continue;
                    case '\t':
                        json_string.Append("\\t"); continue;
                    default: // \u
                        json_string.Append(c); continue;
                }
            }
            json_string.Append("\"");

            return json_string.ToString();
        }
    }
}
