﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

using vJine.Core.IoC;
using vJine.Core.ORM;

namespace vJine.Core.IO.Bin {

    public partial class BinHelper<T> {

        public BinHelper() {
        }

        Stream binaryStream = null;
        public BinHelper(Stream binaryStream) {
            this.binaryStream = binaryStream;
        }

        public int Write(T data) {
            return this.Write(data, this.binaryStream);
        }

        public int Write(T data, Stream stream) {
            if (stream == null) {
                throw new ArgumentNullException("stream", "stream is Null");
            }

            BinHelper<T>.ToString(data, stream);

            return -1;
        }

        public T Read() {
            if (this.binaryStream == null) {
                throw new CoreException("binaryStream is Null");
            }

            return BinHelper<T>.Parse(this.binaryStream);
        }

        public T Read(Stream stream) {
            if (stream == null) {
                throw new ArgumentNullException("stream", "stream is Null");
            }

            return BinHelper<T>.Parse(stream);
        }

        public void Read(Stream stream, T data) {
            BinHelper<T>.Parse(stream, data);
        }

        public void Flush() {
            if (this.binaryStream == null) {
                throw new CoreException("binaryStream is Null");
            }

            this.binaryStream.Flush();
        }

        static readonly Type T_nullable = typeof(Nullable<int>).GetGenericTypeDefinition();
        static readonly Type Tobj = typeof(T);
        static readonly Type Tthis = typeof(BinHelper<T>).GetGenericTypeDefinition();

        public static readonly Exec<T, Stream> toStringHelper = null;
        public static readonly Call<T, Stream, T> parseHelper = null;

        static BinHelper() {
            try {
                BinHelper<T>.toStringHelper = BinHelper<T>.ToString(Class<T>.GetMap());
            } catch (Exception ex) {
                BinHelper<T>.toStringHelper = (T T_obj, Stream stream) => {
                    throw ex;
                };
            }

            try {
                BinHelper<T>.parseHelper = BinHelper<T>.Parse(Class<T>.GetMap());
            } catch (Exception ex) {
                BinHelper<T>.parseHelper = (Stream stream, T data) => {
                    throw ex;
                };
            }
        }

        public static byte[] ToString(T binaryObj) {
            MemoryStream mmStream = new MemoryStream();
            BinHelper<T>.toStringHelper(binaryObj, mmStream);
            return mmStream.ToArray();
        }

        public static void ToString(T binaryObj, Stream stream) {
            BinHelper<T>.toStringHelper(binaryObj, stream);
        }

        public static Exec<T, Stream> ToString(params Class<T>.Property[] P) {
            DynamicMethod dmToString =
                new DynamicMethod("", Reflect.@void, new Type[] { BinHelper<T>.Tobj, Reflect.@Stream }, true);
            MethodInfo stream_write = 
                typeof(Stream).GetMethod("Write", new Type[] { Reflect.@byteArray, Reflect.@int, Reflect.@int });
            Dictionary<Type, LocalBuilder> lvs = new Dictionary<Type, LocalBuilder>();

            ILGenerator ilGen = dmToString.GetILGenerator();

            Exec<bool> gen_write_isNull = (bool IsNull) => {
                ilGen.Emit(IsNull ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.Emit(OpCodes.Call, BinHelperCache.GetBytes(Reflect.@bool));
            };

            #region gen_properties

            Exec _genPropertyToString = () => {
                for (int i = 0, len = P.Length; i < len; i++) {
                    Class<T>.Property p_i = P[i];
                    Type p_type =
                        p_i.IsNullable ? p_i.pTypeNull : p_i.pType;

                    if (p_i.IsXmlIgnore) {
                        continue;
                    }
                    
                    Label lbNextProperty = ilGen.DefineLabel();
                    if(p_i.IsPrimitive) {
                        MethodInfo m_getBytes = null;
                        if (p_i.IsEnum) {
                            Type enum_type = Enum.GetUnderlyingType(p_type);
                            m_getBytes =
                                BinHelperCache.GetBytes(enum_type);
                        } else {
                            m_getBytes = BinHelperCache.GetBytes(p_type);
                        }
                        if (m_getBytes == null) {
                            throw new CoreException("[{0}]:m_getBytes Is Nullable", Reflect.GetTypeName(p_i.pType));
                        }
                        //get_Value
                        if (p_i.IsNullable) {
                            Label lbHasValue = ilGen.DefineLabel();
                            LocalBuilder lvNull = ilGen.DeclareLocal(p_i.pType);
                            Type Tnull = T_nullable.MakeGenericType(p_i.pTypeNull);

                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen, p_i.get);
                            ilGen.Emit(OpCodes.Stloc, lvNull);
                            ilGen.Emit(OpCodes.Ldloca, lvNull);
                            ilGen.Emit(OpCodes.Call, Tnull.GetProperty("HasValue").GetGetMethod());
                            ilGen.Emit(OpCodes.Brtrue, lbHasValue);
                            gen_write_isNull(true);
                            ilGen.Emit(OpCodes.Br, lbNextProperty);

                            ilGen.MarkLabel(lbHasValue);
                            gen_write_isNull(false);
                            ilGen.Emit(OpCodes.Ldloca, lvNull);
                            ilGen.Emit(OpCodes.Call, Tnull.GetProperty("Value").GetGetMethod());
                        } else {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen, p_i.get);
                        }

                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, m_getBytes);
                    } else {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        Emit.Opc_Call(ilGen, p_i.get);

                        MethodInfo binary_tostring =
                            BinHelper<T>.Tthis.MakeGenericType(p_type).GetMethod("ToString", new Type[] { p_type, Reflect.@Stream });

                        ilGen.Emit(OpCodes.Ldarg_1);
                        Emit.Opc_Call(ilGen, binary_tostring);
                    }
                    ilGen.MarkLabel(lbNextProperty);
                }
            };
            #endregion gen_properties

            if (Reflect.IsBaseType(BinHelper<T>.Tobj)) {
                MethodInfo m_getBytes = null;
                if (Reflect.IsEnum(BinHelper<T>.Tobj)) {
                    Type enum_type = Enum.GetUnderlyingType(BinHelper<T>.Tobj);
                    Emit.Opc_Conv(ilGen, enum_type);
                    m_getBytes =
                        BinHelperCache.GetBytes(enum_type);
                } else {
                    m_getBytes = BinHelperCache.GetBytes(BinHelper<T>.Tobj);
                }
                if (m_getBytes == null) {
                    throw new CoreException("[{0}]:m_getBytes Is Nullable", Class<T>.FullName);
                }

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldarg_1);
                ilGen.Emit(OpCodes.Call, m_getBytes);
            } else {
                Label lbIsNotNull = ilGen.DefineLabel();
                {//if(objT == null) return write 1;
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Ldnull);
                    ilGen.Emit(OpCodes.Ceq);

                    ilGen.Emit(OpCodes.Brfalse, lbIsNotNull);
                    {
                        gen_write_isNull(true);
                        ilGen.Emit(OpCodes.Ret);
                    }
                } // else write 0 & value {
                ilGen.MarkLabel(lbIsNotNull); 
                gen_write_isNull(false);
                {
                    if (BinHelper<T>.Tobj == Reflect.@object) {
                        Exec<object, Stream> obj_toString = (object obj, Stream stream) => {
                            Type T_obj = obj.GetType();

                            BinHelper<string>.ToString(Reflect.GetTypeName(T_obj), stream);

                            Type T_objToString = BinHelper<T>.Tthis.MakeGenericType(T_obj);
                            Delegate D =
                                T_objToString.GetField("toStringHelper", BindingFlags.Public | BindingFlags.Static).GetValue(null) as Delegate;
                            D.DynamicInvoke(obj, stream);
                        };

                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, obj_toString.Method);
                    } else if (Reflect.IsArray(BinHelper<T>.Tobj)) {
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ldlen);
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, BinHelperCache.GetBytes(Reflect.@int));

                        if (BinHelper<T>.Tobj == Reflect.@byteArray) {
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldc_I4_0);
                            ilGen.Emit(OpCodes.Ldarg_0); ilGen.Emit(OpCodes.Ldlen);
                            ilGen.Emit(OpCodes.Callvirt,
                                typeof(Stream).GetMethod("Write", new Type[] { typeof(byte[]), typeof(int), typeof(int) }));
                        } else {
                            MethodInfo array_tostring =
                                BinHelperCache.array_toString.MakeGenericMethod(BinHelper<T>.Tobj.GetElementType());
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            Emit.Opc_Call(ilGen, array_tostring);
                        }
                    } else {
                        _genPropertyToString();

                        if (Reflect.IsList(BinHelper<T>.Tobj)) {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen,
                                BinHelper<T>.Tobj.GetProperty("Count").GetGetMethod(true));
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Call,
                                BinHelperCache.GetBytes(Reflect.@int));

                            MethodInfo list_tostring =
                                BinHelperCache.list_toString.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<T>.Tobj));
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            Emit.Opc_Call(ilGen, list_tostring);
                        } else if (Reflect.IsDictionary(BinHelper<T>.Tobj)) {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen,
                                BinHelper<T>.Tobj.GetProperty("Count").GetGetMethod(true));
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Call,
                                BinHelperCache.GetBytes(Reflect.@int));

                            MethodInfo dict_tostring =
                                       BinHelperCache.dict_toString.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<T>.Tobj));
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            Emit.Opc_Call(ilGen, dict_tostring);
                        }
                    }
                }
            }

            ilGen.Emit(OpCodes.Ret);
            return dmToString.CreateDelegate(typeof(Exec<T, Stream>)) as Exec<T, Stream>;
        }
    }

    public partial class BinHelper<T> {

        public static T Parse(Stream stream) {
            return BinHelper<T>.parseHelper(stream, default(T));
        }

        public static void Parse(Stream stream, T data) {
            BinHelper<T>.parseHelper(stream, data);
        }

        public static Call<T, Stream, T> Parse(params Class<T>.Property[] P) {
            DynamicMethod dm_parse =
                new DynamicMethod("", BinHelper<T>.Tobj, new Type[] { Reflect.@Stream, BinHelper<T>.Tobj }, true);

            ILGenerator ilGen = dm_parse.GetILGenerator();

            LocalBuilder lvIsNull = ilGen.DeclareLocal(Reflect.@byteArray);
            ilGen.Emit(OpCodes.Ldc_I4_1);
            ilGen.Emit(OpCodes.Newarr, Reflect.@byte);
            ilGen.Emit(OpCodes.Stloc, lvIsNull);

            MethodInfo m_utility_read = 
                new Call<int, Stream, byte[], int>(Utility.Read).Method;
            Exec gen_read_isNull = () => {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldloc, lvIsNull);
                ilGen.Emit(OpCodes.Ldc_I4_1);
                ilGen.Emit(OpCodes.Call, m_utility_read);
                ilGen.Emit(OpCodes.Pop);

                ilGen.Emit(OpCodes.Ldloc, lvIsNull);
                ilGen.Emit(OpCodes.Ldc_I4_0);
                ilGen.Emit(OpCodes.Ldelem_U1);
                ilGen.Emit(OpCodes.Ldc_I4_1);
                ilGen.Emit(OpCodes.Ceq);
            };

            MethodInfo m_get_len =
                    BinHelperCache.GetValue(Reflect.@int);
            Exec gen_read_len = () => {
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, m_get_len);
            };

            #region gen_properties

            Exec<LocalBuilder> _genPropertyParse = (LocalBuilder lvInstance) => {

                for (int i = 0, len = P.Length; i < len; i++) {
                    Class<T>.Property p_i = P[i];
                    if (p_i.IsXmlIgnore) {
                        continue;
                    }

                    Type p_type =
                        p_i.IsNullable ? p_i.pTypeNull : p_i.pType;

                    if (p_i.IsPrimitive) {
                        MethodInfo m_getValue = null;
                        if (p_i.IsEnum) {
                            m_getValue =
                                BinHelperCache.GetValue(Enum.GetUnderlyingType(p_type));
                        } else {
                            m_getValue = BinHelperCache.GetValue(p_type);
                        }
                        if (m_getValue == null) {
                            throw new CoreException("[{0}]:m_getValue Is Nullable", Reflect.GetTypeName(p_i.pType));
                        }

                        ilGen.Emit(OpCodes.Ldloc, lvInstance);
                        {
                            if (p_i.IsNullable) {
                                Label lbIsNull = ilGen.DefineLabel();
                                Label lbIsNotNull = ilGen.DefineLabel();
                                gen_read_isNull();
                                ilGen.Emit(OpCodes.Brtrue, lbIsNull);
                                ilGen.Emit(OpCodes.Ldarg_0);
                                ilGen.Emit(OpCodes.Call, m_getValue);
                                ilGen.Emit(OpCodes.Newobj, p_i.ctorNull);
                                ilGen.Emit(OpCodes.Br, lbIsNotNull);

                                ilGen.MarkLabel(lbIsNull);
                                {
                                    LocalBuilder lvNull = ilGen.DeclareLocal(p_i.pType);
                                    ilGen.Emit(OpCodes.Ldloca, lvNull);
                                    ilGen.Emit(OpCodes.Initobj, p_i.pType);
                                    ilGen.Emit(OpCodes.Ldloc, lvNull);
                                }

                                ilGen.MarkLabel(lbIsNotNull);
                            } else {
                                ilGen.Emit(OpCodes.Ldarg_0);
                                ilGen.Emit(OpCodes.Call, m_getValue);
                            }
                        }
                        ilGen.Emit(OpCodes.Call, p_i.set);
                    } else {
                        MethodInfo binary_parse =
                            Tthis.MakeGenericType(p_type).GetMethod("Parse", new Type[] { Reflect.@Stream });

                        ilGen.Emit(OpCodes.Ldloc, lvInstance);
                        {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Call, binary_parse);
                        }
                        ilGen.Emit(OpCodes.Call, p_i.set);
                    }
                }
            };
            #endregion gen_properties

            if (Reflect.IsBaseType(BinHelper<T>.Tobj)) {
                MethodInfo m_getValue = null;
                if (Reflect.IsEnum(BinHelper<T>.Tobj)) {
                    m_getValue =
                        BinHelperCache.GetValue(Enum.GetUnderlyingType(BinHelper<T>.Tobj));
                } else {
                    m_getValue = BinHelperCache.GetValue(BinHelper<T>.Tobj);
                }
                if (m_getValue == null) {
                    throw new CoreException("[{0}]:m_getValue Is Nullable", Class<T>.FullName);
                }

                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Call, m_getValue);
            } else {
                Label lbIsNotNull = ilGen.DefineLabel();
                gen_read_isNull();
                {//if(objT == null) return null;
                    ilGen.Emit(OpCodes.Brfalse, lbIsNotNull);
                    ilGen.Emit(OpCodes.Ldnull);
                    ilGen.Emit(OpCodes.Ret);
                } // else return value {
                ilGen.MarkLabel(lbIsNotNull);
                {
                    if (BinHelper<T>.Tobj == Reflect.@object) {
                        Call<object, Stream, object> obj_parse = (Stream stream, object data) => {
                            string type_name = BinHelper<string>.Parse(stream);
                            Type T_obj = Reflect.GetType(type_name);
                            if (T_obj == Reflect.@object) {
                                throw new CoreException("Type Shouldn't Be Object");
                            }

                            //TODO:检查IBinarizer接口
                            Type T_objToString = BinHelper<T>.Tthis.MakeGenericType(T_obj);
                            Delegate D =
                                T_objToString.GetField("parseHelper", BindingFlags.Public | BindingFlags.Static).GetValue(null) as Delegate;
                            return D.DynamicInvoke(stream, data);
                        };

                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Call, obj_parse.Method);
                    } else if (Reflect.IsArray(BinHelper<T>.Tobj)) {
                        int array_rank = BinHelper<T>.Tobj.GetArrayRank();
                        if (array_rank != 1) {
                            throw new CoreException("Just 1 Dimension Array Supported!");
                        }

                        Type T_item = BinHelper<T>.Tobj.GetElementType();
                        LocalBuilder lvArray =
                            ilGen.DeclareLocal(BinHelper<T>.Tobj);
                        LocalBuilder lv_array_len = ilGen.DeclareLocal(Reflect.@int);
                        {
                            gen_read_len();
                            ilGen.Emit(OpCodes.Stloc, lv_array_len);

                            ilGen.Emit(OpCodes.Ldloc, lv_array_len);
                            ilGen.Emit(OpCodes.Newarr, T_item);
                            ilGen.Emit(OpCodes.Stloc, lvArray);
                        }

                        if (BinHelper<T>.Tobj == Reflect.@byteArray) {
                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Ldloc, lvArray);
                            ilGen.Emit(OpCodes.Ldloc, lv_array_len);
                            ilGen.Emit(OpCodes.Call, m_utility_read);
                            ilGen.Emit(OpCodes.Pop);
                        } else {
                            MethodInfo array_parse =
                                BinHelperCache.array_parse.MakeGenericMethod(BinHelper<T>.Tobj.GetElementType());
                            ilGen.Emit(OpCodes.Ldloc, lvArray);
                            ilGen.Emit(OpCodes.Ldloc, lv_array_len);
                            ilGen.Emit(OpCodes.Ldarg_0);
                            Emit.Opc_Call(ilGen, array_parse);
                        }

                        ilGen.Emit(OpCodes.Ldloc, lvArray);
                    } else {
                        LocalBuilder lvInstance =
                            ilGen.DeclareLocal(BinHelper<T>.Tobj);
                        Label lbInstanceIsNotNull = ilGen.DefineLabel();
                        Label lbParseInstance = ilGen.DefineLabel();

                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Ldnull);
                        ilGen.Emit(OpCodes.Ceq);
                        ilGen.Emit(OpCodes.Brfalse, lbInstanceIsNotNull);
                        //if (instance is null) {
                        {//Init Instance
                            Emit.Opc_Init(ilGen, BinHelper<T>.Tobj);
                            ilGen.Emit(OpCodes.Stloc, lvInstance);
                            ilGen.Emit(OpCodes.Br, lbParseInstance);
                        } //} else {
                        {//Load Instance
                            ilGen.MarkLabel(lbInstanceIsNotNull);
                            ilGen.Emit(OpCodes.Ldarg_1);
                            ilGen.Emit(OpCodes.Stloc, lvInstance);
                        }
                        ilGen.MarkLabel(lbParseInstance);
                        {
                            _genPropertyParse(lvInstance);

                            if (Reflect.IsList(BinHelper<T>.Tobj)) {
                                MethodInfo list_parse =
                                    BinHelperCache.list_parse.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<T>.Tobj));
                                ilGen.Emit(OpCodes.Ldloc, lvInstance);
                                gen_read_len();
                                ilGen.Emit(OpCodes.Ldarg_0);
                                Emit.Opc_Call(ilGen, list_parse);
                            } else if (Reflect.IsDictionary(BinHelper<T>.Tobj)) {
                                MethodInfo dict_parse =
                                    BinHelperCache.dict_parse.MakeGenericMethod(Reflect.GetGenericArgs(BinHelper<T>.Tobj));
                                ilGen.Emit(OpCodes.Ldloc, lvInstance);
                                gen_read_len();
                                ilGen.Emit(OpCodes.Ldarg_0);
                                Emit.Opc_Call(ilGen, dict_parse);
                            }
                        }

                        ilGen.Emit(OpCodes.Ldloc, lvInstance);
                    }
                }
            }

            ilGen.Emit(OpCodes.Ret);
            return dm_parse.CreateDelegate(typeof(Call<T, Stream, T>)) as Call<T, Stream, T>;
        }
    }
}
