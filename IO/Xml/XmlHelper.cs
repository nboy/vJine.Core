﻿using System.IO;
using System.Xml;

namespace vJine.Core.IO.Xml {
    public partial class XmlHelper {
        public static string ToString<T>(T xmlObject) where T : class, new() {
            string Name = typeof(T).Name;
            XmlDocument xmlDoc = XmlHelper.LoadXml(Name);
            XmlNode xmlRoot = xmlDoc.DocumentElement;

            XmlHelper.ToString<T>(xmlObject, xmlDoc, xmlRoot);

            string xml = xmlDoc.InnerXml;
            xmlDoc = null;

            return xml;
        }

        public static void ToString<T>(T xmlObject, string xml_file) where T : class, new() {
            string Name = typeof(T).Name;
            XmlDocument xmlDoc = XmlHelper.LoadXml(Name);
            XmlNode xmlRoot = xmlDoc.DocumentElement;

            XmlHelper.ToString<T>(xmlObject, xmlDoc, xmlRoot);
            xmlDoc.Save(xml_file);
        }

        public static void ToString<T>(T xmlObject, Stream xml_stream) where T : class, new() {
            string Name = typeof(T).Name;
            XmlDocument xmlDoc = XmlHelper.LoadXml(Name);
            XmlNode xmlRoot = xmlDoc.DocumentElement;

            XmlHelper.ToString<T>(xmlObject, xmlDoc, xmlRoot);
            xmlDoc.Save(xml_stream);
        }

        public static T Parse<T>(string xml_file) where T : class, new() {
            return XmlHelper.Parse<T>(xml_file, false);
        }

        public static T Parse<T>(string xml, bool IsText) where T : class, new() {
            XmlDocument xmlDoc = new XmlDocument();
            if (IsText) {
                xmlDoc.LoadXml(xml);
            } else {
                xmlDoc.Load(xml);
            }

            return XmlHelper.Parse<T>(xmlDoc.DocumentElement);
        }

        public static void Parse<T>(string xml_file, T xml_object) where T : class, new() {
            XmlHelper.Parse<T>(xml_file, xml_object, false);
        }

        public static void Parse<T>(string xml, T xml_object, bool IsText) where T : class, new() {
            XmlDocument xmlDoc = new XmlDocument();
            if (IsText) {
                xmlDoc.LoadXml(xml);
            } else {
                xmlDoc.Load(xml);
            }

            XmlHelper.Parse<T>(xmlDoc.DocumentElement, xml_object);
        }

        public static T Parse<T>(Stream xml_stream) where T : class, new() {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_stream);

            return XmlHelper.Parse<T>(xmlDoc.DocumentElement);
        }

        public static void Parse<T>(Stream xml_stream, T xml_object) where T : class, new() {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xml_stream);

            XmlHelper.Parse<T>(xmlDoc.DocumentElement, xml_object);
        }

        static XmlDocument LoadXml(string Name) {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(
                string.Format("<?xml version=\"1.0\" encoding=\"utf-8\" ?><{0}></{0}>", Name)
                );

            return xmlDoc;
        }
    }
}