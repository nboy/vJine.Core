﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.IO.Xml {
    public class XmlException : Exception {
        public XmlException() {
        }

        public XmlException(Exception ex)
            : base(ex.Message, ex.InnerException) {

        }

        public XmlException(string Msg, params object[] Args)
            : base(string.Format(Msg, Args)) {

        }

        public XmlException(Exception inner, string Msg, params object[] Args)
            : base(string.Format(Msg, Args), inner) {
        }
    }
}
