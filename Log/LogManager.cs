﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using vJine.Core.Task;
using System.IO;
using System.Reflection;
using System.Collections;

namespace vJine.Core.Log {
    public class LogManager : IDisposable {
        public string log_dir { get; private set; }
        public int log_max { get; private set; }
        TaskQueue<string> log_pipe = null;
        public LogManager(string log_dir, int log_max) {
            if (!Directory.Exists(log_dir)) {
                Directory.CreateDirectory(log_dir);
            }

            this.log_dir = log_dir;
            this.log_max = log_max;

            this.log_pipe =
                new TaskQueue<string>(1000, this.log_writer);
            this.log_pipe.Start();
        }

        Stream logStream = null; StreamWriter logWriter = null;
        int log_file_counter = 0, log_counter = 0;
        DateTime log_start = DateTime.Now;
        void log_writer(string log) {
            string log_file = "";

        next_log: ;
            if (logWriter == null) {
                goto get_log_file;
            }
            if (this.log_counter > this.log_max) {
                this.log_counter = 0;
                goto get_log_file;
            }
            if (DateTime.Now.AddDays(-1) > log_start) {
                goto get_log_file;
            }

            logWriter.WriteLine(log); logWriter.Flush();

            this.log_counter += 1;

            return;
        get_log_file: ;
            if (this.logWriter != null) {
                this.logWriter.Close();
                this.logWriter = null; this.logStream = null;
            }

            this.log_start = DateTime.Now;
            for (int i = 0; ; i++) {
                log_file =
                    Path.Combine(this.log_dir, string.Format("{0:yyMMdd}_{1:000}.log", this.log_start, i));
                if (!File.Exists(log_file)) {
                    log_file_counter = i;
                    break;
                }
            }

            this.logStream =
                new FileStream(log_file, FileMode.CreateNew, FileAccess.Write, FileShare.Read);
            this.logWriter = new StreamWriter(this.logStream);

            goto next_log;
        }

        public void I(params string[] info) {
            this.log_pipe.Enqueue("[I]," + string.Join(",", info));
        }

        public void W(params string[] warning) {
            this.log_pipe.Enqueue("[W]," + string.Join(",", warning));
        }

        public void E(params string[] error) {
            this.log_pipe.Enqueue("[E]," + string.Join(",", error));
        }

        public void E(string tag, Exception ex) {
            this.log_pipe.Enqueue("[E], " + LogManager.dump("***", ex));
        }

        public void F(params string[] fatal) {
            this.log_pipe.Enqueue("[F]," + string.Join(",", fatal));
        }

        public void Dispose() {
            this.log_pipe.Stop();

            if (this.logWriter != null) {
                this.logWriter.Flush();
                this.logWriter.Close();
                this.logWriter = null; this.logStream = null;
            }
        }

        #region Helper
        public static string dump(string Tag, Exception ex) {
            StringBuilder sbEx = new StringBuilder(2000);
            dump(Tag, sbEx, ex); return sbEx.ToString();
        }

        static void dump(string Tag, StringBuilder sbEx, Exception ex) {
            sbEx.Append("=============" + Tag + "=============" + Environment.NewLine);
            sbEx.AppendFormat("Message:{0}" + Environment.NewLine, ex.Message);

            sbEx.AppendFormat("Data:" + Environment.NewLine);
            foreach (DictionaryEntry de in ex.Data) {
                sbEx.AppendFormat("Key:{0},Value:{1}" + Environment.NewLine, de.Key.ToString(), de.Value.ToString());
            }

            sbEx.AppendFormat("Help Link:{0}" + Environment.NewLine, ex.HelpLink);

            sbEx.AppendFormat("Source:{0}" + Environment.NewLine, ex.Source);
            sbEx.AppendFormat("StackTrace:" + Environment.NewLine + "{0}" + Environment.NewLine, ex.StackTrace);
            if (ex.TargetSite != null) {
                sbEx.AppendFormat("TargetSite:{0}@{1}" + Environment.NewLine, ex.TargetSite.Name, ex.TargetSite.DeclaringType.ToString());
            }

            if (ex.InnerException != null) {
                dump(Tag, sbEx, ex.InnerException);
            }
        }
        #endregion
    }
}
