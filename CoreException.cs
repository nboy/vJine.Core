﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace vJine.Core {
    public class CoreException : Exception {
        public CoreException()
            : base() {

        }

        public CoreException(Exception ex)
            : base("", ex) {

        }

        public CoreException(string Msg, params object[] Args)
            : base(string.Format(Msg, Args)) {
        }

        public CoreException(Exception inner, string Msg, params object[] Args)
            : base(string.Format(Msg, Args), inner) {
        }

        public CoreException(MethodBase method, string Msg, params object[] Args)
            : base(string.Format(Msg, Args)) {
        }

        public static void Throw() {
            throw new CoreException();
        }

        public static void Throw(Exception ex) {
            throw new CoreException(ex);
        }

        public static void Throw(string Msg, params object[] Args) {
            throw new CoreException(Msg, Args);
        }

        public static void Throw(Exception inner, string Msg, params object[] Args) {
            throw new CoreException(inner, Msg, Args);
        }

        public static void Throw(MethodBase method, string Msg, params object[] Args) {
            throw new CoreException(method, Msg, Args);
        }
    }
}
