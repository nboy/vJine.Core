﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vJine.Core.Context
{
    public class ContextException : Exception
    {
        public ContextException(string Msg, params object[] Args)
            : base(string.Format(Msg, Args))
        {

        }
    }

    public class BizContextException : ContextException
    {
        public BizContextException(string Msg, params object[] Args)
            : base(Msg, Args)
        {

        }
    }

    public class SysContextException : ContextException
    {
        public SysContextException(string Msg, params object[] Args)
            : base(Msg, Args)
        {

        }
    }
}
