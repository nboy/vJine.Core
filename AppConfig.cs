﻿using System;
using System.Configuration;
using System.Xml;

using vJine.Core.IO.Xml;
using vJine.Core.IoC;
using vJine.Core.ORM;

namespace vJine.Core {
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class AppConfigAttribute : Attribute {
        public AppConfigAttribute(string sectionName) {
            this.sectionName = sectionName;
        }

        public string sectionName { get; protected set; }
    }

    public class AppConfig<T> : IConfigurationSectionHandler where T : class, new() {
        public object Create(object parent, object configContext, XmlNode section) {
            return XmlHelper.Parse<T>(section);
        }

        static readonly string _AssemblyQualifiedName;
        public static string AssemblyQualifiedName {
            get {
                return AppConfig<T>._AssemblyQualifiedName;
            }
        }

        static readonly string _FullName;
        public static string FullName {
            get {
                return AppConfig<T>._FullName;
            }
        }

        static readonly string sectionName = null;
        static AppConfig() {
            AppConfig<T>._AssemblyQualifiedName = Class<AppConfig<T>>.AssemblyQualifiedName; ;
            AppConfig<T>._FullName = Class<AppConfig<T>>.FullName;

            Type typeConfig = typeof(T);
            AppConfigAttribute[] appConfigs = Reflect.GetAttribute<AppConfigAttribute>(typeConfig);
            if (appConfigs == null || appConfigs.Length == 0) {
                AppConfig<T>.sectionName = null;
            } else {
                AppConfig<T>.sectionName = appConfigs[0].sectionName;
            }
        }

        public static T Get() {
            string sectionName = AppConfig<T>.sectionName;
            if (string.IsNullOrEmpty(sectionName)) {
                throw new CoreException("sectionName is Null:Type:[{0}]", Class<T>.FullName);
            }

            return AppConfig<T>.Get(sectionName) as T;
        }

        public static T Get(string section_name) {
            return ConfigurationManager.GetSection(section_name) as T; 
        }
    }
}
